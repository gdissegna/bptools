<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'logo'     => 'Logo',
    
    /*
   |--------------------------------------------------------------------------
   | META
   |--------------------------------------------------------------------------
   */

    'site-title' => 'BP Tools - leader italiano nella produzione di utensili per la lavorazione del legno',
    'keywords' => 'Keywords',
    'description' => 'Description',
    
   /*
   |--------------------------------------------------------------------------
   | Header
   |--------------------------------------------------------------------------
   */
    
    'contattaci' => 'Contattaci',
    
    
    
    
   /*
   |--------------------------------------------------------------------------
   | Menu
   |--------------------------------------------------------------------------
   */

    'home' => 'Home',
    'azienda' => 'Azienda',
    'prodotti' => 'Prodotti',
    'diamante' => 'Frese in Diamante',
    'coltellino' => 'Frese a coltellino',
    'widia' => 'Frese in Widia',
    'contatti' => 'Contatti',
    'carrello' => 'Carrello quotazioni',
    
    /*
   |--------------------------------------------------------------------------
   | Newsletter
   |--------------------------------------------------------------------------
   */

    'newsletter_testo' => 'Iscriviti alla nostra newsletter per essere sempre aggiornato.',
    'iscrizione' => 'Iscriviti alla Newsletter',
    'iscrizione_button' => 'Iscriviti!',
    'newsletter_ok' => 'Sei stato aggiunto alla nostra lista.',
    
    
   /*
   |--------------------------------------------------------------------------
   | Home
   |--------------------------------------------------------------------------
   */

    'categorie' => 'Categorie',
    'in_vetrina' => 'In vetrina',
    'partners' => 'Partners',
    'nostro_catalogo' => 'Il nostro catalogo',
    'clicca' => 'Clicca',
    'qui' => 'qui',
    'catalogo_download' => 'per scaricare il nostro Catalogo prodotti in formato PDF',
    'claim_attivita' => 'Lavorazione utensili per il legno',
    'attivita_text_home' => 'BP TOOLS opera nel settore della costruzione di utensili speciali per la lavorazione del legno e derivati, metallo, carta e materie plastiche. La nostra attività è frutto dello sviluppo di realtà aziendali presenti nel mercato nazionale ed internazionale con esperienza pluriennale.',
    'prodotti_evidenza' => 'Prodotti in evidenza',
    'le_nostre' => 'Le nostre',
    'news' => 'News',
    'leggi_tutto' => 'Leggi tutto',
    //'offerte' => 'Offerte',
    'offerte' => 'Ultime notizie',
    'entra_contatto' => 'Entra in Contatto con Noi!',
    'seguici' => 'Seguici',
        

    
    /*
   |--------------------------------------------------------------------------
   | Prodotti
   |--------------------------------------------------------------------------
   */

    'title_articolo' => 'Articolo',
    'download_catalogo' => 'Download catalogo 2017',
    'download_file' => 'Download file',
    'link_utili' => 'Link utili',
    'carrello_button' => 'Aggiungi al carrello',
    'prodotto_aggiunto' => 'Prodotto aggiunto al carrello',
    
    
    
    /*
   |--------------------------------------------------------------------------
   | Contatti
   |--------------------------------------------------------------------------
   */

    'contatti_title' => 'Contattateci',
    'contatti_form_title' => 'Richiesta informazioni',
    'contatti_form_header' => 'Per ogni informazione a cui sei interessato, compila il modulo che vedi qui riportato: il nostro Staff ti contatterà per fornirti ogni indicazione                                       necessaria. I campi contrassegnati con un asterisco *, sono obbligatori. ',
    'contatti_subtitle' => 'Scriveteci, telefonateci: il nostro Staff per Voi',
    'contatti_indirizzo' => 'Indirizzo',
    'contatti_contatti' => 'Contatti',
    'contatti_contatto' => 'Contatto',
    'contatti_telefono' => 'Telefono',
    'label_nome' => 'Nome e cognome',
    'label_societa' => 'Società',
    'label_indirizzo' => 'Indirizzo',
    'label_citta' => 'Città',
    'label_paese' => 'Paese',
    'label_telefono' => 'Telefono',
    'label_fax' => 'Fax',
    'label_email' => 'Il tuo indirizzo Email',
    'label_destinatario' => 'Destinatario della richiesta',
    'label_vendite' => 'Ufficio vendite',
    'label_uff_tecnico' => 'Ufficio tecnico',
    'label_amministrazione' => 'Ufficio amministrazione',
    'label_logistica' => 'Logistica',
    'label_altro' => 'Altro',    
    'label_rivenditore' => 'Rivenditore',
    'label_utilizzatore' => 'Utilizzatore',
    'label_costruttore' => 'Costruttore',
    'label_messaggio' => 'Messaggio',
    'label_brief_privacy' => 'Dichiaro di accettare la Privacy Policy leggibile',
    'label_privacy_testo' => 'In riferimento al D.lgs 30 giugno 2003, n. 196 (codice in materia di protezione dei dati personali), Le comunichiamo che il trattamento dei suoi dati                                   personali è finalizzato alla gestione di questa operazione, all\'invio di nostro materiale informativo e all\'utilizzo dei dati da parte di Tecnomec                                     S.r.l. Il Titolare del trattamento dei dati è Tecnomec S.r.l. Lei può esercitare i diritti di cui all\'Art. 7 del D.lgs 196/03 (tra cui i diritti di                                     accesso, rettifica, aggiornamento e cancellazione) rivolgendosi a Tecnomec S.r.l., P.IVA IT00953430303, Via Aquileia, 58 (SS305), 34071 Cormons (GO), Tel.                               +39.0481.676679, Fax +39.0481.676681, E-mail: info@tecnomecsrl.it.',
    'label_privacy_ok' => 'Do il mio consenso al trattamento dei dati per i fini sopra descritti. *',
    'label_invia' => 'Invia',
    'entra_in' => 'Entra in',
    'uffici' => 'Uffici',
    
    /*
   |--------------------------------------------------------------------------
   | FORM EMAIL
   |--------------------------------------------------------------------------
   */
    
    'contact_subject' => 'Grazie per averci contattato',
    'cart_form_header' => 'Modulo richiesta quotazione',
    'cart_subject' => 'Riepilogo quotazione',
    'contact_title' => 'Riepilogo informazioni',
    'carrello_prodotti' => 'Prodotti nel carrello',
    'rimuovi_carrello' => 'Elimina',
   
    
    /*
   |--------------------------------------------------------------------------
   | FOOTER
   |--------------------------------------------------------------------------
   */

    'telefono' => 'Telefono',
    'fax' => 'Fax',
    'indirizzo' => 'Indirizzo',
    'copyright' => 'Tutti i diritti riservati.',
    'notelegali' => 'Note legali',
    'privacy' => 'Privacy Policy',
    'credits' => 'Credits',
    
    /*
   |--------------------------------------------------------------------------
   | SOCIAl
   |--------------------------------------------------------------------------
   */

    'facebook' => 'Apri la nostra pagina Facebook',
    'youtube' => 'Il nostro canale su YouTube',
    
   

];