<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'titolo_azienda'     => 'La nostra Azienda',
    'bp_tools'     => 'BP TOOLS',
    'dinamica'     => 'è una realtà dinamica',
    'introduzione'     => 'BP TOOLS opera nel settore della costruzione di utensili speciali per la lavorazione del legno e derivati, metallo, carta e materie plastiche.',
    'testo' => 'La nostra attività è frutto dello sviluppo di realtà aziendali presenti nel mercato nazionale ed internazionale con esperienza pluriennale.
                        La realizzazione degli utensili avviene attraverso un ciclo produttivo preciso e controllato dove l’utilizzo di macchine utensili di ultima generazione, la selezione di materiali di alta qualità, investimenti in tecnologie ed un know-how evoluto, ci consentono di offrire un prodotto qualitativamente elevato e un servizio altamente efficiente.
                        Inoltre l’elasticità produttiva della BP TOOLS permette la progettazione e la realizzazione di utensili speciali che soddisfano le specifiche esigenze di richiesta della clientela.
                        BP TOOLS è composto da un team forte, giovane e dinamico. I nostri utensili sono costruiti interamente in Italia: MADE IN ITALY.',
    'esperienza'     => 'Esperienza pluriennale',
    'ciclo_produttivo'     => 'Ciclo produttivo controllato',
    'il_nostro'     => 'Il nostro',
    'lavoro'     => 'lavoro',
    'ci_trovate'     => 'Ci trovate',
    'qui'     => 'qui',
    'indirizzo'     => 'Indirizzo',
    'telefono'     => 'Telefono',
    'fax'     => 'Fax',
         
   

];