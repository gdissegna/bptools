<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'logo'     => 'Logo',
        
    /*
   |--------------------------------------------------------------------------
   | META
   |--------------------------------------------------------------------------
   */

    'site-title' => 'BP Tools - Woodworking tools manufacturer in San Giovanni al Natisone Udine Italy - Our production consist in Cutters, Insert cutters, Finger-Joint Solutions',
    'keywords' => 'Keywords',
    'description' => 'Description',
    
   
    
    
   /*
   |--------------------------------------------------------------------------
   | Menu
   |--------------------------------------------------------------------------
   */

    'home' => 'Home',
    'azienda' => 'About us',
    'notizie' => 'News',
    'prodotti' => 'Produtcs',
    'diamante' => 'DIA tools',
    'coltellino' => 'Knives cutterheads',
    'widia' => 'Widia cutterheads',
    'contatti' => 'Contact us',
    'carrello' => 'Offers summary',
    'newprod-text' => 'We are proud to present our new article',
    'newprod-desc' => 'SET TENONING CNC MACHINES Ø30/48 ATT.20X50<br>ADJUSTABLE 10÷30',
    
    
     /*
   |--------------------------------------------------------------------------
   | Newsletter
   |--------------------------------------------------------------------------
   */

    'newsletter_testo' => 'Keep up on our always evolving product features and technology. Enter your e-mail and subscribe to our newsletter.',
    'iscrizione' => 'Subscribe to our newsletter.',
    'iscrizione_button' => 'Subscribe!',
    'newsletter_ok' => 'Success! You\'ve been added to our email list.',
    
   /*
   |--------------------------------------------------------------------------
   | Home
   |--------------------------------------------------------------------------
   */
    
    'categorie' => 'Categories',
    'in_vetrina' => 'Featured',
    'partners' => 'Partners',
    'nostro_catalogo' => 'Our catalogue',
    'clicca' => 'Clicca',
    'qui' => 'qui',
    'catalogo_download' => 'to download our Pdf Catalogue',
    'claim_attivita' => 'Woodworking tools',
    'attivita_text_home' => 'BP TOOLS realises special tools necessary to work wood and its derived products, metals, paper and plastics. Our company is the result of the cooperation and development with important national and international industries with several years of experience.',
    'prodotti_evidenza' => 'Featured products',
    'le_nostre' => 'Our',
    'news' => 'News',
    'leggi_tutto' => 'Read more',
    //'offerte' => 'Offers',
    'offerte' => 'Lastest News',

     /*
   |--------------------------------------------------------------------------
   | Prodotti
   |--------------------------------------------------------------------------
   */

    'title_articolo' => 'Article',
    'download_catalogo' => 'Download catalogo 2017',
    'download_file' => 'Download file',
    'link_utili' => 'Link utili',
    'carrello_button' => 'Add to cart',
    'prodotto_aggiunto' => 'Prodotto aggiunto al carrello',
    
    /*
   |--------------------------------------------------------------------------
   | Contatti
   |--------------------------------------------------------------------------
   */

    'contatti_title' => 'Contact us',
    'contatti_form_title' => 'Information Reguest',
    'contatti_form_header' => 'For further information, please fill in the form hereunder. Our staff will contact you. Fields marked with * are mandatory. ',
    'contatti_subtitle' => 'Write or phone, our Staff is at your disposal',
    'contatti_indirizzo' => 'Address',
    'contatti_contatti' => 'Contact us',
    'contatti_contatto' => 'with us',
    'contatti_telefono' => 'Phone',
    'label_nome' => 'First and last Name',
    'label_societa' => 'Company',
    'label_indirizzo' => 'Address',
    'label_citta' => 'City',
    'label_paese' => 'Country',
    'label_telefono' => 'Phone',
    'label_fax' => 'Fax',
    'label_email' => 'Your Email address',
    'label_destinatario' => 'Type of your request',
    'label_vendite' => 'Sales Office',
    'label_uff_tecnico' => 'Technical department',
    'label_amministrazione' => 'Administration department',
    'label_logistica' => 'Logistic department',
    'label_altro' => 'Other',
    'label_attivita' => 'Type of activity',
    'label_rivenditore' => 'Reseller',
    'label_utilizzatore' => 'User',
    'label_costruttore' => 'Manufacturer',
    'label_messaggio' => 'Message',
    'label_brief_privacy' => 'I declare to accept Privacy Policy readable',
    'label_privacy_testo' => 'According to the Legislative Decree n. 196/2003 (personal data protection code), we inform you that your personal data will be processed with the purpose                               of providing the services required (website surfing, information request, sign-in for services) and of sending advertising and promotion information                                     and/or material. Tecnomecsrl S.r.l. IS RESPONSIBLE FOR THE DATA PROCESSING. You may exercise the rights provided by art.7 of Legislative Decree n.                                       196/2003 by writing to: Tecnomecsrl S.r.l. P.IVA IT00953430303, Via Aquileia, 58 (SS305), 34071 Cormons (GO), Phone +39.0481.676679, Fax +39.0481.676681,                               E-mail: info@tecnomecsrl.it.',
    'label_privacy_ok' => 'I hereby grant my consent to the processing of my personal data according to the above stated purposes. *',
    'label_invia' => 'Send now',
    'entra_in' => 'Get in touch',
    'uffici' => 'Our Offices',
    
    /*
   |--------------------------------------------------------------------------
   | FORM EMAIL
   |--------------------------------------------------------------------------
   */
    
    'contact_subject' => 'Thanks for contacting us',
    'cart_form_header' => 'Quotation request form',
    'cart_subject' => 'Cart summary',
    'contact_title' => 'Information summary',
    'carrello_prodotti' => 'Cart products',
    'rimuovi_carrello' => 'Remove',
    
   
    
    /*
   |--------------------------------------------------------------------------
   | FOOTER
   |--------------------------------------------------------------------------
   */

    'telefono' => 'Phone',
    'fax' => 'Fax',
    'indirizzo' => 'Address',
    'copyright' => 'All rights reserved.',
    'notelegali' => 'Legal disclaimer',
    'privacy' => 'Privacy Policy',
    'credits' => 'Credits',
       
/*
   |--------------------------------------------------------------------------
   | SOCIAl
   |--------------------------------------------------------------------------
   */

    'facebook' => 'Connect to our Facebook page',
    'youtube' => 'Watch our YouTube channel',
    

];