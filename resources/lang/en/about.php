<?php


return [

    /*
   |--------------------------------------------------------------------------
   | Custom Validation Attributes
   |--------------------------------------------------------------------------
   |
   | The following language lines are used to swap attribute place-holders
   | with something more reader friendly such as E-Mail Address instead
   | of "email". This simply helps us make messages a little cleaner.
   |
   */

    'titolo_azienda'     => 'Our Company',
    'bp_tools'     => 'BP TOOLS',
    'dinamica'     => 'is a dynamic company',
    'introduzione'     => 'BP Tools realises special tools necessary to work wood and its derived products, metals, paper and plastics.',
    'testo' => 'Our company is the result of the cooperation and development with important national and international industries with several years of experience. The realisation of our tools develops along a precise and controlled production cycle where the latest generation of machines, the choice of high quality materials, investments in high efficiency technology and advanced know-how, permits to present a product and services with high standards. Furthermore the working flexibility of BP Tools allows the realisation of special utensils that completely satisfy the client’s needs. BP Tools is a strong, young and dynamic team. Our tools are entirely realised in Italy: MADE IN ITALY.',
    'esperienza'     => 'Long-time experience',
    'ciclo_produttivo'     => 'Controlled production cycle',
    'il_nostro'     => 'Our',
    'lavoro'     => 'work',
    'ci_trovate'     => 'Find us',
    'qui'     => 'here',
    'indirizzo'     => 'Address',
    'telefono'     => 'Phone',
    'fax'     => 'Fax',
         
   

];