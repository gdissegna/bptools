@extends('Front.master')

@section('content')
    @include('Front._partials.header')
    <div role="main" class="main">

        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">Prodotti</li>
                            <li class="active">Frese</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1>FRESE</h1>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">

            <div class="row">
                <div class="col-md-3">
                    <aside class="sidebar">

                        <h4 class="heading-primary">Categorie</h4>
                        <ul class="nav nav-list mb-xlg">
                            @foreach($menu_categories as $category)
                                <li><a href="{{ route('category', $category->slug) }}">{{ $category->name }}</a></li>
                            @endforeach
                        </ul>

                        <div class="tabs mb-xlg">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#popularPosts" data-toggle="tab"><i class="fa fa-star"></i>
                                        In vetrina</a></li>

                            </ul>
                        </div>

                        <hr>

                        <h4 class="heading-primary">Il nostro Catalogo</h4>
                        <p>Clicca <a href="{{ assett('bptools-catalog-2016.pdf') }}" target="_blank">qui</a> per scaricare il nostro
                            Catalogo prodotti in formato PDF. </p>

                    </aside>
                </div>
                <div class="col-md-9">

                    <h2>I nostri articoli</h2>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="lead">
                                Vi presentiamo una anteprima del nostro catalogo articoli; prossimanente troverete tutti
                                gli articoli. </p>
                        </div>
                    </div>

                    <hr class="tall">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">

                                <ul class="portfolio-list sort-destination" data-sort-id="portfolio">
                                    @foreach($all_categories as $category)
                                    <li class="col-md-4 isotope-item brands">
                                        <div class="portfolio-item">
                                            <a href="categoria-frese_it.html">
                                            <span class="thumb-info thumb-info-lighten">
                                                <span class="thumb-info-wrapper">
                                                <img src="{{ $category->getFirstMediaUrl('images') }}"
                                                     class="img-responsive" alt="">
                                                <span class="thumb-info-title">
                                                    <span class="thumb-info-inner">{{ $category->name }}</span>
                                                    <span class="thumb-info-type">Clicca per entrare</span>
                                                </span>
                                                <span class="thumb-info-action">
                                                <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                                                </span>
                                            </span>
                                            </span> </a>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
@endsection