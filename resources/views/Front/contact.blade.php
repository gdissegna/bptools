@extends('Front.master')



@section('content')

    <div role="main" class="main">

        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">Contattaci</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1>{{ trans('custom.contattaci') }}</h1>
                    </div>
                </div>
            </div>
        </section>

        <!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
        <div id="googlemaps" class="google-map"></div>

        <div class="container">

            <div class="row">
                <div class="col-md-6">
                    <h2 class="mb-sm mt-sm"><strong>{{ trans('custom.contattaci') }}</strong></h2>
                    <div class="col-md-12">
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    </div>
                    <form class="form-contact-alt" id="contact-form-alt" action="{{ route('Front.postContact') }}" method="post" >
                        <input class="hidden" name="_token" type="hidden" value="{{ csrf_token() }}">
                        <input type="hidden" value="true" name="emailSent" id="emailSent">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-6">
                                    <label>{{ trans('custom.label_nome') }} *</label>
                                    <input type="text" value="" data-msg-required="Inserisci il tuo nome e cognome." maxlength="100" class="form-control" name="name" id="name" required>
                                </div>
                                <div class="col-md-6">
                                    <label>{{ trans('custom.label_email') }} *</label>
                                    <input type="email" value="" data-msg-required="Inserisci il tuo indirizzo email." data-msg-email="Inserisci un indirizzo email valido." maxlength="100" class="form-control" name="email" id="email" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>{{ trans('custom.label_indirizzo') }} *</label>
                                    <input type="text" value="" data-msg-required="Inserisci il tuo indirizzo." data-msg-email="Inserisci un indirizzo valido." maxlength="100" class="form-control" name="address" id="address" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>{{ trans('custom.label_citta') }}</label>
                                    <input type="text" value="" data-msg-required="Inserisci la tua città." data-msg-email="Inserisci una città valida." maxlength="100" class="form-control" name="city" id="city" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>{{ trans('custom.label_paese') }}</label>
                                    <input type="text" value="" data-msg-required="Inserisci il tuo Paese." data-msg-email="Inserisci un Paese valido." maxlength="100" class="form-control" name="country" id="country" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>{{ trans('custom.label_telefono') }}</label>
                                    <input type="text" value="" data-msg-required="Inserisci il tuo telefono." data-msg-email="Inserisci un telefono valido." maxlength="100" class="form-control" name="phone" id="phone" required>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>{{ trans('custom.label_destinatario') }}</label>
                                    <select data-msg-required="{{ trans('custom.label_oggetto') }}" class="form-control" name="subject" id="subject" required>
                                        <option value="">...</option>
                                        <option value="Sales">{{ trans('custom.label_vendite') }}</option>
                                        <option value="Technical">{{ trans('custom.label_uff_tecnico') }}</option>
                                        <option value="Administrative">{{ trans('custom.label_amministrazione') }}</option>
                                        <option value="Logistic">{{ trans('custom.label_logistica') }}</option>
                                        <option value="Other">{{ trans('custom.label_altro') }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <label>{{ trans('custom.label_messaggio') }} </label>
                                    <textarea maxlength="5000" rows="6" class="form-control" name="message" id="message"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <label>* {{ trans('custom.label_brief_privacy') }} <a href="">{{ trans('custom.qui') }}</a></label>
                            </div>
                        </div>
                        <div class="row">
                            {!! View::make('recaptcha::display') !!}
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <hr>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="submit" id="contactFormSubmit" value="Invia" class="btn btn-primary btn-lg pull-right" data-loading-text="Caricamento...">
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-md-6">

                    <h4 class="heading-primary mt-lg">{{ trans('custom.entra_in') }}  <strong>{{ trans('custom.contatti_contatto') }}</strong></h4>
                    <p class="lead">{{ trans('custom.contatti_form_header') }}</p>
                    <p></p>
                    <hr>
                    <h4 class="heading-primary"> <strong>{{ trans('custom.uffici') }}</strong></h4>
                    <ul class="list list-icons list-icons-style-3 mt-xlg">
                        <li><i class="fa fa-map-marker"></i> <strong>{{ trans('custom.indirizzo') }}:</strong> Viale delle Scuole 74, San Giovanni al Natisone, Udine Italia</li>
                        <li><i class="fa fa-phone"></i> <strong>{{ trans('custom.telefono') }}:</strong> (+39) 0432.756437</li>
                        <li><i class="fa fa-fax"></i> <strong>{{ trans('custom.fax') }}:</strong> (+39) 0432.756437</li>
                        <li><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:info@bptools-srl.com">info@bptools-srl.com</a></li>
                    </ul>

                </div>
            </div>

        </div>

    </div>

@endsection