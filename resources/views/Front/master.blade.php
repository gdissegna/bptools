<!DOCTYPE html>
<html>

<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <title>BP TOOLS Srl Woodworking tools | @yield('title')</title>

    <meta name="keywords" content=""/>
    <meta name="description" content="@yield('seo_decription')">
    <meta name="author" content="Omega Web Agency">

    <!-- Favicon -->
    <link rel="shortcut icon" href="Front/img/favicon.ico" type="image/x-icon"/>
    <link rel="apple-touch-icon" href="Front/img/apple-touch-icon.png">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Web Fonts  -->
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light"
          rel="stylesheet" type="text/css">

    @include('Front._partials.css')

<!-- Head Libs -->
    <script src="{{ asset('Front/js/modernizr.min.js') }}"></script>

</head>

<body>
<div class="body">


@yield('content')

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="footer-ribbon">
                    <span>{{ trans('custom.entra_contatto') }}</span>
                </div>
                <div class="col-md-6">
                    <div class="newsletter">
                        <h4>Newsletter</h4>
                        <p>{{ trans('custom.newsletter_testo') }}</p>

                        <div class="alert alert-success hidden" id="newsletterSuccess">
                            <strong>OK!</strong> {{ trans('custom.newsletter_ok') }}
                        </div>

                        <div class="alert alert-danger hidden" id="newsletterError"></div>

                        <form id="newsletterForm" action="php/newsletter-subscribe.php" method="POST">
                            <div class="input-group">
                                <input class="form-control col-md-10" placeholder="Email Address" name="newsletterEmail"
                                       id="newsletterEmail" type="text">
                                <span class="input-group-btn">
											<button class="btn btn-default" type="submit">{{ trans('custom.iscrizione_button') }}</button>
										</span>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="contact-details">
                        <h4>{{ trans('custom.contattaci') }}</h4>
                        <ul class="contact">
                            <li>
                                <p><i class="fa fa-map-marker"></i> <strong>{{ trans('custom.indirizzo') }}:</strong> Viale delle Scuole n. 74, San
                                    Giovanni al Natisone, Udine Italia</p>
                            </li>
                            <li>
                                <p><i class="fa fa-phone"></i> <strong>{{ trans('custom.telefono') }}:</strong> (+39) 0432.756437</p>
                            </li>
                            <li>
                                <p><i class="fa fa-fax"></i> <strong>{{ trans('custom.fax') }}:</strong> (+39) 0432.756437</p>
                            </li>
                            <li>
                                <p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:info@bptools-srl.com">info@bptools-srl.com</a></p>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-2">
                    <h4>{{ trans('custom.seguici') }}</h4>
                    <ul class="header-social-icons social-icons hidden-xs">
              <li class="social-icons-facebook"><a href="https://www.facebook.com/Bp-Tools-Woodworking-1211560892226180/" target="_blank" title="{{ trans('custom.facebook') }}"><i class="fa fa-facebook"></i></a></li>
            </ul>
            <ul class="header-social-icons social-icons hidden-xs">
              <li class="social-icons-youtube"><a href="https://www.youtube.com/channel/UC8GIS7YVNYeMHLJcGZggOYw" target="_blank" title="{{ trans('custom.youtube') }}"><i class="fa fa-youtube"></i></a></li>
            </ul>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                        <a href="index.html" class="logo">
                            <img alt="Porto Website Template" class="img-responsive" src="img/logo-footer.png">
                        </a>
                    </div>
                    <div class="col-md-7">
                        <p>© Copyright {{ date('Y') }}. {{ trans('custom.copyright') }}</p>
                    </div>
                    <div class="col-md-4">
                        <nav id="sub-menu">
                            <ul>
                                <li><a href="sitemap.html">Sitemap</a></li>
                                <li><a href="contact-us.html">{{ trans('custom.contatti') }}</a></li>
                                <li><a href="page-faq.html">Note legali</a></li>
                                <li><a href="page-faq.html">Privacy</a></li>
                                <li><a href="page-faq.html">Credits</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </footer>
</div>
@include('Front._partials.js')
</body>

</html>