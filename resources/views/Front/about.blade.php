@extends('Front.master')

@include('Front._partials.header')

@section('content')

    <div role="main" class="main">

        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">La nostra Azienda</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1>{{ trans('about.titolo_azienda') }}</h1>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="owl-carousel owl-theme mb-none" data-plugin-options='{"items": 1, "animateOut": "fadeOut", "autoplay": true, "autoplayTimeout": 3000}'>
                        <div>
                            <img alt="" class="img-responsive img-rounded" src="img/our-office-1.jpg">
                        </div>
                        <div>
                            <img alt="" class="img-responsive img-rounded" src="img/our-office-1.jpg">
                        </div>
                        <div>
                            <img alt="" class="img-responsive img-rounded" src="img/our-office-3.jpg">
                        </div>
                    </div>
                </div>
            </div>

            <hr class="tall">

            <div class="row">
                <div class="col-md-7">
                    <h2>{{ trans('about.bp_tools') }} <strong> {{ trans('about.dinamica') }}</strong></h2>
                    <p class="lead">{{ trans('about.introduzione') }}</p>

                    <p>{{ trans('about.testo') }}</p>

                    <div class="row mt-xlg mb-xl">
                        <div class="col-md-6">
                            <div class="feature-box feature-box-style-2">
                                <div class="feature-box-icon">
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="feature-box-info">
                                    <h4 class="heading-primary mb-xs">{{ trans('about.esperienza') }}</h4>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="feature-box feature-box-style-2">
                                <div class="feature-box-icon">
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="feature-box-info">
                                    <h4 class="heading-primary mb-xs">{{ trans('about.ciclo_produttivo') }}</h4>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <hr class="tall">

                    <h4>{{ trans('about.il_nostro') }} <strong>{{ trans('about.lavoro') }}</strong></h4>

                    <div data-plugin-lightbox data-plugin-options='{"delegate": "a", "type": "image", "gallery": {"enabled": true}}'>
                        <div class="row">
                            <div class="col-md-4">
                                <a class="img-thumbnail mb-xl" href="img/our-office-4.jpg" data-plugin-options='{"type":"image"}'>
                                    <img class="img-responsive" src="img/our-office-4.jpg" alt="Office">
                                    <span class="zoom">
												<i class="fa fa-search"></i>
											</span>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a class="img-thumbnail mb-xl" href="img/our-office-5.jpg" data-plugin-options='{"type":"image"}'>
                                    <img class="img-responsive" src="img/our-office-5.jpg" alt="Office">
                                    <span class="zoom">
												<i class="fa fa-search"></i>
											</span>
                                </a>
                            </div>
                            <div class="col-md-4">
                                <a class="img-thumbnail mb-xl" href="img/our-office-6.jpg" data-plugin-options='{"type":"image"}'>
                                    <img class="img-responsive" src="img/our-office-6.jpg" alt="Office">
                                    <span class="zoom">
												<i class="fa fa-search"></i>
											</span>
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-md-5">
                    <h4>{{ trans('about.ci_trovate') }} <strong>{{ trans('about.qui') }}</strong></h4>

                    <!-- Google Maps - Go to the bottom of the page to change settings and map location. -->
                    <div id="googlemaps" class="google-map small"></div>

                    <ul class="list list-icons list-icons-style-3 mt-xlg">
                        <li><i class="fa fa-map-marker"></i> <strong>{{ trans('about.indirizzo') }}:</strong> Viale delle Scuole, 74 San Giovanni al Natisone, Udine Italy</li>
                        <li><i class="fa fa-phone"></i> <strong>{{ trans('about.telefono') }}:</strong> +39.0432.756437</li>
                        <li><i class="fa fa-fax"></i> <strong>{{ trans('about.fax') }}:</strong> +39.0432.756437</li>
                        <li><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:info@bptools-srl.com">info@bptools-srl.com</a></li>
                    </ul>
                </div>
            </div>

        </div>

    </div>

@endsection