<div class="slider-container rev_slider_wrapper">
    <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options='{"delay": 4500, "gridwidth": 1170, "gridheight": 500}'>
        <ul>
            @foreach($sliders as $slider)
                <li data-transition="fade">
                    <img src="{{ $slider->getFirstMediaUrl('images') }}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg">
                </li>
            @endforeach
        </ul>
    </div>
</div>