<!-- INIZIO Header Section -->
<header id="header" class="header-mobile-nav-only"
        data-plugin-options='{"stickyEnabled": true, "stickyEnableOnBoxed": true, "stickyEnableOnMobile": true, "stickyStartAt": 90, "stickySetTop": "-90px", "stickyChangeLogo": true}'>
    <div class="header-body">
        <div class="header-top">
            <div class="container">
                <p>
                    {{ trans('custom.contattaci') }} <span class="ml-xs"><i class="fa fa-phone"></i> (+39) 0432.756437</span><span
                            class="hidden-xs"> | <a href="mailto:info@bptools-srl.com">info@bptools-srl.com</a></span>
                </p>
                <nav class="header-nav-top" style="float:right;">
                    <ul class="nav nav-pills">
                        <li>
                            <a href="#" class="dropdown-menu-toggle" id="dropdownLanguage" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                <img src="img/blank.gif" class="flag flag-us" alt="{{ LaravelLocalization::getCurrentLocale() }}" /> {{ LaravelLocalization::getCurrentLocaleName() }}
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownLanguage">
                                @foreach(LaravelLocalization::getSupportedLocales() as $localeCode => $properties)
                                <li>
                                    <a href="{{LaravelLocalization::getLocalizedURL($localeCode) }}"><img src="img/blank.gif" class="flag flag-{{ $localeCode }}" alt="{{ $localeCode }}" /> {{ $properties['native']  }}</a>
                                </li>
                                @endforeach
                            </ul>
                        </li>
                    </ul>
                </nav>
                <ul class="header-social-icons social-icons hidden-xs">
              <li class="social-icons-facebook"><a href="https://www.facebook.com/Bp-Tools-Woodworking-1211560892226180/" target="_blank" title="{{ trans('custom.facebook') }}"><i class="fa fa-facebook"></i></a></li>
            </ul>
            <ul class="header-social-icons social-icons hidden-xs">
              <li class="social-icons-youtube"><a href="https://www.youtube.com/channel/UC8GIS7YVNYeMHLJcGZggOYw" target="_blank" title="{{ trans('custom.youtube') }}"><i class="fa fa-youtube"></i></a></li>
            </ul>
            </div>
        </div>
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-logo">
                        <a href="index.html">
                            <img alt="Porto" width="111" height="54" data-sticky-width="82" data-sticky-height="40"
                                 data-sticky-top="29" src="img/logo.png">
                        </a>
                    </div>
                </div>
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-nav header-nav-stripe">
                            <button class="btn header-btn-collapse-nav" data-toggle="collapse"
                                    data-target=".header-nav-main">
                                <i class="fa fa-bars"></i>
                            </button>
                            <div class="header-nav-main header-nav-main-square header-nav-main-effect-1 header-nav-main-sub-effect-1 collapse">
                                <nav>
                                    <ul class="nav nav-pills" id="mainNav">
                                        <li>
                                            <a href="{{ route('Front.Index') }}">Home</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('Front.About') }}">Azienda</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('Front.listCategories') }}">Prodotti</a>
                                        </li>
                                        <li>
                                            <a href="#">Novità</a>
                                        </li>
                                        <li>
                                            <a href="#">Offerte</a>
                                        </li>
                                        <li>
                                            <a href="">Dove siamo</a>
                                        </li>
                                        <li>
                                            <a href="{{ route('Front.Contact') }}">Contatti</a>
                                        </li>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<!-- /Fine Header Section -->