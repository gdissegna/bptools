@extends('Front.master')

@section('content')
    <div class="main" role="main">
        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li>
                                <a href="index.html">Home</a>
                            </li>
                            <li class="active">Prodotti</li>
                            <li class="active">Frese</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1>{{ $cat_products->name }}</h1>
                    </div>
                </div>
            </div>
        </section>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <aside class="sidebar">
                        <h4 class="heading-primary">{{ trans('custom.categorie') }}</h4>
                        <ul class="nav nav-list mb-xlg">
                            <li>
                                <a href="categoria-frese_it.html">Frese</a>
                            </li>
                        </ul>
                        <div class="tabs mb-xlg">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a data-toggle="tab" href="#popularPosts"><i class="fa fa-star"></i> {{ trans('custom.prodotti_evidenza') }}</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="popularPosts">
                                    <ul class="simple-post-list">
                                        <li>
                                            <div class="post-image">
                                                <div class="img-thumbnail">
                                                    <a href="blog-post.html"><img alt=""
                                                                                  src="img/blog/prod-thumb-1.jpg"></a>
                                                </div>
                                            </div>
                                            <div class="post-info">
                                                <a href="blog-post.html">Fresa m</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <hr>
                        <h4 class="heading-primary">{{ trans('custom.nostro_catalogo') }}</h4>
                        <p>{{ trans('custom.clicca') }} <a href="bptools-catalog-2016.pdf" target="_blank">{{ trans('custom.qui') }}</a> {{ trans('custom.catalogo_download') }}.</p>
                    </aside>
                </div>
                <div class="col-md-9">
                    <h2>FRESE</h2>
                    <div class="row">
                        <div class="col-md-12">
                            <p class="lead">{!!  $cat_products->description !!}</p>
                        </div>
                    </div>
                    <hr class="tall">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <ul class="portfolio-list sort-destination" data-sort-id="portfolio">
                                    @foreach($cat_products->products as $product)
                                    <li class="col-md-4 isotope-item brands">
                                        <div class="portfolio-item">
                                            <a href="{{ route('product',[$product->category->slug, $product->slug]) }}">
                                            <span class="thumb-info thumb-info-lighten"><span
                                                        class="thumb-info-wrapper">
                                            <img alt="" class="img-responsive"
                                                 src="{{ $product->getFirstMediaUrl('images', 'Thumb') }}">
                                            <span class="thumb-info-title"><span class="thumb-info-inner">{{ $product->name }}</span></span>
                                            <span class="thumb-info-action"><span class="thumb-info-action-icon"><i
                                                            class="fa fa-link"></i></span></span></span></span></a>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection