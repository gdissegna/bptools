@extends('Front.master')

@section('content')

    @include('Front._partials.header')

    <!-- INIZIO main section -->
    <div role="main" class="main">

        @include('Front._partials.slider')

        <div class="container">

            <div class="row">
                <div class="col-md-3">
                    <aside class="sidebar">

                        <h4 class="heading-primary">{{ trans('custom.categorie') }}</h4>
                        <ul class="nav nav-list mb-xlg">
                            @foreach($menu_categories as $category)
                                <li><a href="{{ route('Front.Category', $category->slug) }}">{{ $category->name }}</a></li>
                            @endforeach
                        </ul>

                        <div class="tabs mb-xlg">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#popularPosts" data-toggle="tab"><i class="fa fa-star"></i>
                                        {{ trans('custom.in_vetrina') }}</a></li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="popularPosts">
                                    <ul class="simple-post-list">
                                        @foreach($home_articles as $article)
                                        <li>
                                            <div class="post-image">
                                                <div class="img-thumbnail">
                                                    <a href="blog-post.html"> <img src="{{ $article->getFirstMediaUrl('images') }}"
                                                                                   alt=""> </a>
                                                </div>
                                            </div>
                                            <div class="post-info">
                                                <a href="{{ route() }}">{{ $article->title }}</a>
                                            </div>
                                        </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <h4 class="heading-primary">{{ trans('custom.nostro_catalogo') }}</h4>
                        <p>{{ trans('custom.clicca') }} <a href="bptools-catalog-2016.pdf" target="_blank">{{ trans('custom.qui') }}</a> {{ trans('custom.catalogo_download') }}.</p>

                    </aside>
                </div>
                <div class="col-md-9">

                    <h2>{{ trans('custom.claim_attivita') }}</h2>

                    <div class="row">
                        <div class="col-md-12">
                            <p class="lead">
                                {{ trans('custom.attivita_text_home') }} </p>
                        </div>
                    </div>

                    <hr class="tall">

                    <div class="row">
                        <div class="col-md-12">
                            <h2>{{ trans('custom.prodotti_evidenza') }} </h2>
                            <hr>
                            <div class="row">
                                <ul class="portfolio-list sort-destination" data-sort-id="portfolio">
                                    @foreach($home_products as $product)
                                    <li class="col-md-4 isotope-item brands">
                                        <div class="portfolio-item">
                                            <a href="{{ route('Front.Product',['category' => $product->Category->slug, 'product' => $product->slug]) }}">
                                    <span class="thumb-info thumb-info-lighten">
                                        <span class="thumb-info-wrapper">
                                        <img src="{{ $product->getFirstMediaUrl('images', 'Thumb') }}"
                                                                    class="img-responsive" alt="">
                                <span class="thumb-info-title">	<span class="thumb-info-inner">{{ $product->name }}</span>
                                </span>
                                <span class="thumb-info-action">
                                <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                                </span>
                                </span>
                                    </span> </a>
                                        </div>
                                    </li>
                                    @endforeach
                                </ul>
                            </div>

                        </div>
                    </div>

                </div>

            </div>

        </div>

    </div>

    <section class="section mt-none section-footer clearfix">
        <div class="container">
            <div class="row">
                <div class="col-md-12 center">
                    <h2>{{ trans('custom.le_nostre') }} <strong> {{ trans('custom.news') }}</strong></h2>
                </div>
            </div>
            <div class="row mt-lg">
                @foreach($home_articles as $article)
                <div class="col-md-3">
                    <img class="img-responsive" src="{{ $article->getFirstMediaUrl('images', 'Thumb') }}" alt="Blog">
                    <div class="recent-posts mt-md mb-lg">
                        <article class="post">
                            <h5><a class="text-dark" href="blog-post.html">{{ $article->title }}.</a></h5>
                            <p>{{ $article->body }}</p>
                            <div class="post-meta">
                                <span><i class="fa fa-calendar"></i> {{ $article->updated_at }} </span>

                            </div>
                        </article>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>

    <div class="col-md-12 margin-top-bottom">
        <div class="container">
            <h4>{{ trans('custom.partners') }}</h4>
            <div class="owl-carousel owl-theme stage-margin rounded-nav"
                 data-plugin-options='{"margin": 10, "loop": false, "nav": true, "dots": false, "stagePadding": 40}'>
                <div>
                    <img class="img-responsive" src="img/logos/blank.png" alt="">
                </div>
                <div>
                    <img class="img-responsive" src="img/logos/logo-cnt.png" alt="">
                </div>
                <div>
                    <img class="img-responsive" src="img/logos/blank.png" alt="">
                </div>
            </div>
        </div>
    </div>

    </div>
    <!-- /Fine Main Section -->

@endsection