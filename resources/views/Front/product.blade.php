@extends('Front.master')

@include('Front._partials.header')

@section('content')

    <div role="main" class="main">
        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="index.html">Home</a></li>
                            <li class="active">Prodotti</li>
                            <li class="active">Frese</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1>{{  $product->category->name }}</h1>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">

            <div class="row">
                <div class="col-md-3">
                    <aside class="sidebar">

                        <h4 class="heading-primary">Categorie</h4>
                        <ul class="nav nav-list mb-xlg">
                            @foreach($menu_categories as $category)
                                <li><a href="{{ route('Front.Category', $category->slug) }}">{{ $category->name }}</a>
                                </li>
                            @endforeach
                        </ul>

                        <div class="tabs mb-xlg">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#popularPosts" data-toggle="tab"><i class="fa fa-star"></i>
                                        In vetrina</a></li>

                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="popularPosts">
                                    <ul class="simple-post-list">
                                        <li>
                                            <div class="post-image">
                                                <div class="img-thumbnail">
                                                    <a href="blog-post.html"> <img src="img/blog/prod-thumb-1.jpg"
                                                                                   alt=""> </a>
                                                </div>
                                            </div>
                                            <div class="post-info">
                                                <a href="blog-post.html">Fresa m</a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="post-image">
                                                <div class="img-thumbnail">
                                                    <a href="blog-post.html"> <img src="img/blog/prod-thumb-2.jpg"
                                                                                   alt=""> </a>
                                                </div>
                                            </div>
                                            <div class="post-info">
                                                <a href="blog-post.html">Fresa multipla</a>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="post-image">
                                                <div class="img-thumbnail">
                                                    <a href="blog-post.html"> <img src="img/blog/prod-thumb-1.jpg"
                                                                                   alt=""> </a>
                                                </div>
                                            </div>
                                            <div class="post-info">
                                                <a href="blog-post.html">Fresa multipla</a>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                        <hr>

                        <h4 class="heading-primary">Il nostro Catalogo</h4>
                        <p>Clicca <a href="bptools-catalog-2016.pdf" target="_blank">qui</a> per scaricare il nostro
                            Catalogo prodotti in formato PDF. </p>

                    </aside>
                </div>
                <div class="col-md-9">
                    <h1>{{ $product->name }}</h1>
                    <p>{!! $product->description !!}  </p>
                </div>
                <div class="col-md-9">
                    <div class="row">
                        <div class="lightbox"
                             data-plugin-options='{"delegate": "a.lightbox-portfolio", "type": "image", "gallery": {"enabled": true}}'>
                            <ul class="portfolio-list">
                                <li class="col-md-12">
                                    <div class="portfolio-item">
                      <span class="thumb-info thumb-info-centered-icons thumb-info-no-borders">
                        <span class="thumb-info-wrapper">
                        <img src="{{  $product->getMedia('images')->first()->getUrl() }}" class="img-responsive" alt="{{ $product->getMedia('images')->first()->getCustomProperties('alttext') }}">
                        <a href="{{  $product->getMedia('images')->first()->getUrl() }}" class="lightbox-portfolio">
                          <span class="thumb-info-action">
                            <span class="thumb-info-action-icon thumb-info-action-icon-light"><i
                                        class="fa fa-search-plus"></i></span>
                      </span>
                      </a>
                      </span>
                      </span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="lightbox"
                             data-plugin-options='{"delegate": "a.lightbox-portfolio", "type": "image", "gallery": {"enabled": true}}'>
                            <ul class="portfolio-list">
                                @foreach($product->getMedia('images') as $image)
                                <li class="col-md-4">
                                    <div class="portfolio-item">
                      <span class="thumb-info thumb-info-centered-icons thumb-info-no-borders">
                        <span class="thumb-info-wrapper">
                        <img src="{{ $image->getUrl() }}" title="sample_text" class="img-responsive" alt="{{ $image->getCustomProperty('alttext') }}">
                        <a href="{{ $image->getUrl() }}" title="@if($localeCode = "IT") {{ $image->getCustomProperty('note_it') }} @else {{ $image->getCustomProperty('note_en') }} @endif" class="lightbox-portfolio">
                          <span class="thumb-info-action">
                            <span class="thumb-info-action-icon thumb-info-action-icon-light">
                                <i class="fa fa-search-plus"></i></span>
                          </span>
                        </a>
                      </span>
                      </span>
                                    </div>
                                </li>
                                @endforeach
                            </ul>
                        </div><!-- fine immagini scheda prodotto -->
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-9">
                        @foreach($product->ProductSku as $sku)
                            <h4>{{ $sku->name }}</h4>

                            <table class="table table-striped">
                                <thead>
                                <tr>
                                    @foreach ($sku->ColumnTitles as $name)
                                        <th>
                                            {{ $name }}
                                        </th>
                                    @endforeach
                                </tr>
                                </thead>
                                <tbody>
                                @foreach(onfig::get( 'SkuDataTables' ) as $skudata)
                                    @foreach($sku->ProductSkuData as $productskudata)
                                        @if ( $productskudata->$skudata != null )
                                            <tr>
                                                <td>
                                                    {{ $productskudata->$skudata  }}
                                                </td>
                                                <td>
                                                    <form class="form-contact-alt cart" id="contact-form-alt" action=""
                                                          method="post">
                                                        <imput type="hidden" name="codice_articolo"
                                                               value="{{ $productskudata->codice_articolo }}">
                                                            <input type="text" name="quantita" value="0"> <input
                                                                    type="submit"
                                                                    value="{{ trans('custom.inviaform') }}">
                                                    </form>
                                                </td>
                                            </tr>
                                        @endif
                                    @endforeach
                                @endforeach
                                </tbody>
                            </table>
                        @endforeach

                    </div>
                </div>
            </div>

@endsection