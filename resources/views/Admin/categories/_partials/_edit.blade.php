{{ csrf_field() }}
@foreach(LaravelLocalization::getSupportedLanguagesKeys() as $lang)
    <div class="form-group">
        <label for="name_{{ $lang }}" class="col-sm-2 control-label">Nome Categoria {{ $lang }}</label>
        <div class="col-sm-10">
            <input name="name_{{ $lang }}" class="form-control" id="name_{{ $lang }}"
                   placeholder="Nome {{ $lang }}" value="" type="text">
        </div>
    </div>
@endforeach
@foreach(LaravelLocalization::getSupportedLanguagesKeys() as $lang)
    <div class="form-group">
        <label for="description_{{ $lang }}" class="col-sm-2 control-label">Descrizione Categoria {{ $lang }}</label>
        <div class="col-sm-10">
                            <textarea name="description_{{ $lang }}" class="form-control tinymce" id=""
                                      placeholder="Descrizione {{ $lang }}" type="text" content=""></textarea>
        </div>
    </div>
@endforeach
<div class="form-group">
    <label for="" class="col-sm-2 control-label">Seleziona Categoria Parente</label>
    <div class="col-sm-10">
        <select name="parent" class="form-control">
            <option value="0">Nessuna</option>
            @foreach($parent_categories as $parent)
                <option value="{{ $parent->id }}">{{ $parent->name }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <label for="Image" class="col-sm-2 control-label">Aggiungi Immagine</label>
    <div class="col-sm-10">
        <input name="image" id="Image" placeholder="Add Image" type="file">
    </div>
</div>

<button type="submit" class="btn btn-primary pull-right">Submit</button>