@extends('Admin.admin_master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pagina Categorie
            <small>Lista delle categorie</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/Admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="/Admin/Category">Categorie</a></li>
        </ol>
    </section>
    <section class="container">
        @include('vendor.flash.message')
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Title</h3>

                <div class="box-tools pull-right">
                    <a href="{{ route('Admin.Category.create') }}"><button type="button" class="btn btn-primary">Crea nuova Categoria</button></a>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                            <tr>
                                <td>ID</td>
                                <td>Immagine</td>
                                <td>Nome</td>
                                <td>ID parente</td>
                                <td>Opzioni</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($categories as $category)
                            <tr>
                                <td>{{ $category->id }}</td>
                                <td><img src="{{ $category->getFirstMediaUrl('images', 'adminThumb') }}"></td>
                                <td>{{ $category->name }}</td>
                                <td>{{ $category->parent_id }}</td>
                                <td>
                                    <div class="btn-group">
                                        <a href="{{ route('Admin.CategoryImages.index', ['category' => $category->id]) }}">
                                            <button data-original-title="edit_images" type="button" class="btn btn-info btn-sm" data-widget="edita immagini" data-toggle="tooltip" title="">
                                                <i class="fa fa-edit"></i></button></a>
                                        <a href="{{ route('Admin.Category.edit', ['category' => $category->id]) }}">
                                        <button data-original-title="edit" type="button" class="btn btn-info btn-sm" data-widget="edita" data-toggle="tooltip" title="">
                                            <i class="fa fa-edit"></i></button></a>
                                        <a href="{{ route('Admin.Category.destroy',['category' => $category->id]) }}">
                                            <button data-original-title="Remove" type="button" class="btn btn-info btn-sm" data-widget="cancella" data-toggle="tooltip" title="">
                                            <i class="fa fa-remove"></i></button></a>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->

@endsection