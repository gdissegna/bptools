@extends('Admin.admin_master')

@section('content')

        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Gestione Utenti
        <small>gestisci gli utenti per l'accesso all Admin di OmegaCms</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Gestione Slider</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Aggiungi una Slider</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            @include('Admin._partials.errors')
            {!! Form::open(['route' => 'Admin.Slider.store', 'method' => 'post','class' => 'form-horizontal', 'files' => 'true']) !!}
            <div class="box-body">

                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $lang)
                    <div class="form-group">
                        <label for="title_{{ $lang }}" class="col-sm-2 control-label">Titolo Slide {{ $lang }}</label>
                        <div class="col-sm-10">
                            <input name="title_{{ $lang }}" class="form-control" id="title_{{ $lang }}"
                                   placeholder="Title {{ $lang }}" type="text">
                        </div>
                    </div>
                @endforeach

                @foreach(LaravelLocalization::getSupportedLanguagesKeys() as $lang)
                    <div class="form-group">
                        <label for="text_{{ $lang }}" class="col-sm-2 control-label">Testo Slide {{ $lang }}</label>
                        <div class="col-sm-10">
                            <textarea name="text_{{ $lang }}" class="form-control tinymce" id=""
                                      placeholder="Testo {{ $lang }}" type="text"></textarea>
                        </div>
                    </div>
                @endforeach
                <div class="form-group">
                        <label for="Image" class="col-sm-2 control-label">Aggiungi Immagine</label>
                        <div class="col-sm-10">
                            <input name="image" id="Image" placeholder="Add Image" type="file">
                        </div>
                    </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Crea</button>
            </div>
            <!-- /.box-footer -->
            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Lista sliders</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            @include('Admin._partials.flash')
            <hr>
            <div class="box-body">
                <ul class="todo-list ui-sortable">
                    @foreach($Sliders as $slider)
                        <li class="" style="">
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>

                            <img src="{{ $slider->getFirstMediaUrl('images', 'adminThumb') }}">
                            <span class="text">Make the theme responsive</span>
                            <div class="tools">
                                <a href="{{ route('Admin.Slider.destroy', [$slider->id]) }}"><i class="fa fa-trash-o"></i></a>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
</section>
@endsection




