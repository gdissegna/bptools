@extends('Admin.admin_master')

@section('content')

        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Gestione Immagini Prodotto
        <small>Gestisci immagini e downloads per i prodotti</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Gestione immagini Categoria</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Aggiungi un immagine</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            @include('Admin._partials.errors')
            {!! Form::open(['route' => ['Admin.ProductImages.store', $product_id], 'method' => 'post','class' => 'form-horizontal', 'files' => 'true']) !!}
            <div class="box-body">
                <div class="form-group">
                    <label for="Image" class="col-sm-2 control-label">Aggiungi Immagine</label>
                    <div class="col-sm-10">
                        <input name="image" id="image" placeholder="Add Image" type="file">
                    </div>
                </div>
                <div class="form-group">
                    <label for="Image" class="col-sm-2 control-label">Tipologia upload</label>
                    <div class="col-sm-10">
                        <select name="collection" class="form-control">
                            @foreach($product_images->mediaLibraryCollections as $option)
                                <option>{{ $option }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="Image" class="col-sm-2 control-label">Alt Text</label>
                    <div class="col-sm-10">
                        <input name="alt" class="form-control" id="alt" placeholder="Add Alt Text" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label for="Image" class="col-sm-2 control-label">Note IT</label>
                    <div class="col-sm-10">
                        <input name="note_it" class="form-control" id="Note_it" placeholder="Add Note IT" type="text">
                    </div>
                </div>
                <div class="form-group">
                    <label for="Image" class="col-sm-2 control-label">Note EN</label>
                    <div class="col-sm-10">
                        <input name="note_en" class="form-control" id="Note_en" placeholder="Add Note EN" type="text">
                    </div>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right">Crea</button>
            </div>
            <!-- /.box-footer -->
            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Lista immagini</h3>

            <div class="box-tools pull-right">

            </div>
        </div>
        <div class="box-body">
            @include('Admin._partials.flash')
            @include('Admin._partials.errors')
            <hr>
            <div class="box-body">
                <ul class="todo-list ui-sortable">
                    @foreach($product_images->getMedia('images') as $image)
                        <li class="" style="">
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                            <img src="{{ $image->getUrl('adminThumb') }}">
                            <span class="text">{{ $image->getCustomProperty('alttext') }}</span>
                            <div class="tools">
                                <a href="{{ route('Admin.ProductImages.edit',[$image->model_id, $image->id]) }}"><i class="fa fa-edit"></i></a>
                                <a href="{{ route('Admin.ProductImages.delete',[$image->model_id, $image->id]) }}"><i class="fa fa-trash-o"></i></a>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Lista Download</h3>

            <div class="box-tools pull-right">

            </div>
        </div>
        <div class="box-body">
            @include('Admin._partials.flash')
            <hr>
            <div class="box-body">
                <ul class="todo-list ui-sortable">
                    @foreach($product_images->getMedia('downloads') as $file)
                        <li class="" style="">
                      <span class="handle ui-sortable-handle">
                        <i class="fa fa-ellipsis-v"></i>
                        <i class="fa fa-ellipsis-v"></i>
                      </span>
                            <span class="text">{{ $file->filename }}</span>
                            <div class="tools">
                                <i class="fa fa-edit"></i>
                                <i class="fa fa-trash-o"></i>
                            </div>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
</section>

@endsection