@extends('Admin.admin_master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pagina Tabelle Prodotto
            <small>Lista delle tabelle prodotto </small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="/Admin/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="/Admin/Products">Prodotti</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Title</h3>

                <div class="box-tools pull-right">
                    <a href="{{ route('Admin.ProductSku.create', ['Product' => $product_id ]) }}"><button type="button" class="btn btn-primary">Crea nuova tabella</button></a>
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                <div class="table-responsive">
                    <table class="table no-margin">
                        <thead>
                        <tr>
                            <td>ID</td>
                            <td>Name</td>
                            <td>Options</td>
                        </tr>
                        </thead>
                        <tbody>
                        @if($allskus->count() > 0)
                            @foreach($allskus as $sku)
                                <tr>
                                    <td>{{ $sku->id }}</td>
                                    <td>{{ $sku->name }}</td>
                                    <td>
                                        <div class="btn-group">
                                            <a href="{{ route('Admin.ProductSkudata.index', ['Product' => $product_id ,'sku' => $sku->id]) }}">
                                                <button data-original-title="pagina record tabella" type="button" class="btn btn-info btn-sm" data-widget="edita" data-toggle="tooltip" title="">
                                                    <i class="fa fa-edit"></i></button></a>
                                            <a href="{{ route('Admin.ProductSku.edit', ['Product' => $product_id ,'sku' => $sku->id]) }}">
                                                <button data-original-title="edit" type="button" class="btn btn-info btn-sm" data-widget="edita" data-toggle="tooltip" title="">
                                                    <i class="fa fa-edit"></i></button></a>
                                            <a href="{{ route('Admin.ProductSku.delete', ['Product' => $sku->Product->id ,'sku' => $sku->id]) }}">
                                            <button data-original-title="Remove" type="button" class="btn btn-info btn-sm" data-widget="cancella" data-toggle="tooltip" title="">
                                                <i class="fa fa-remove"></i></button></a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->

@endsection