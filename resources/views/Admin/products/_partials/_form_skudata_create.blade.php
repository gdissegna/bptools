{{ csrf_field() }}
    <div class="form-group">
        <label for="codice_articlo" class="col-sm-2 control-label">Codice articolo</label>
        <div class="col-sm-10">
            <input name="codice_articlo" class="form-control" id="codice_articlo" placeholder="Codice articolo">
        </div>
    </div>

@foreach( Config::get('SkuDataTables') as $skudata)
<div class="form-group">

    <label for="" class="col-sm-2 control-label">{{ $skudata }}</label>
    <div class="col-sm-10">
                <input name="{{ $skudata  }}" class="form-control" id="name_{{ $skudata  }}" placeholder="valore {{ $skudata  }}"
                       value="" type="text">
    </div>

</div>
@endforeach

<button type="submit" class="btn btn-primary pull-right">Submit</button>