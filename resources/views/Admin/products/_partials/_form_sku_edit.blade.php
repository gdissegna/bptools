@foreach(LaravelLocalization::getSupportedLanguagesKeys() as $lang)
    <div class="form-group">
        <label for="name_{{ $lang }}" class="col-sm-2 control-label">Nome Prodotto {{ $lang }}</label>
        <div class="col-sm-10">
            <input name="name_{{ $lang }}" class="form-control" id="name_{{ $lang }}" placeholder="Nome {{ $lang }}"
                   value="" type="text">
        </div>
    </div>
@endforeach
<div class="form-group">
    <label for="" class="col-sm-2 control-label">Seleziona tipo tabella (inserisci colonne tabella divise da ,)</label>
    <div class="col-sm-10">
        <input name="colonne" class="form-control" id="colonne" placeholder="colonne"
               value="" type="text">
    </div>
</div>

<button type="submit" class="btn btn-primary pull-right">Submit</button>