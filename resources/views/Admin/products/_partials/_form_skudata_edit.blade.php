{{ csrf_field() }}
@foreach(LaravelLocalization::getSupportedLanguagesKeys() as $lang)
    <div class="form-group">
        <label for="name_{{ $lang }}" class="col-sm-2 control-label">Nome Prodotto {{ $lang }}</label>
        <div class="col-sm-10">
            <input name="name_{{ $lang }}" class="form-control" id="name_{{ $lang }}" placeholder="Nome {{ $lang }}"
                   value="" type="text">
        </div>
    </div>
@endforeach
<div class="form-group">
    <label for="" class="col-sm-2 control-label">Seleziona tipo tabella</label>
    <div class="col-sm-10">
        <select name="parent" class="form-control">
            <option value="0">Nessuna</option>
            @foreach( array_keys(Config::get('SkuDataTables')) as $skudata)
                <option value="{{ $skudata }}">{{ $skudata }}</option>
            @endforeach
        </select>
    </div>
</div>
<button type="submit" class="btn btn-primary pull-right">Submit</button>