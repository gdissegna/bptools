@extends('Admin.admin_master')

@section('content')

    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Pagina creazione Prodotto
            <small>Crea un nuovo prodotto</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Crea Sku Prodotto</a></li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content">

        <!-- Default box -->
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title">Edita sku prodotto</h3>

                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fa fa-minus"></i></button>
                    <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
                        <i class="fa fa-times"></i></button>
                </div>
            </div>
            <div class="box-body">
                @include('Admin._partials.errors')
                <div class="form-horizontal">
                    {!! Form::open(['route' => ['Admin.ProductSku.update',$product_id,$sku_id], 'method' => 'post', 'files' =>'true']) !!}
                    @include('Admin.products._partials._form_sku_edit')
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                Footer
            </div>
            <!-- /.box-footer-->
        </div>
        <!-- /.box -->

    </section>
    <!-- /.content -->

@endsection