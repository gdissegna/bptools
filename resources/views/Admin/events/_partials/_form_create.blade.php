@foreach(LaravelLocalization::getSupportedLanguagesKeys() as $lang)
    <div class="form-group">
        <label for="titolo_{{ $lang }}" class="col-sm-2 control-label">titolo {{ $lang }}</label>
        <div class="col-sm-10">
            <input name="titolo_{{ $lang }}" class="form-control" id="name_{{ $lang }}"
                   placeholder="titolo {{ $lang }}" value="" type="text">
        </div>
    </div>
@endforeach
@foreach(LaravelLocalization::getSupportedLanguagesKeys() as $lang)
    <div class="form-group">
        <label for="body_{{ $lang }}" class="col-sm-2 control-label">Descrizione evento {{ $lang }}</label>
        <div class="col-sm-10">
                            <textarea name="decrizione_{{ $lang }}" class="form-control tinymce" id=""
                                      placeholder="Descrizione {{ $lang }}" type="text" content=""></textarea>
        </div>
    </div>
@endforeach
<div class="form-group">
    <label for="Image" class="col-sm-2 control-label">Aggiungi Evento</label>
    <div class="col-sm-10">
        <input name="image" id="Image" placeholder="Add Image" type="file">
    </div>
</div>

<button type="submit" class="btn btn-primary pull-right">Submit</button>