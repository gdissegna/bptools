@extends('Admin.admin_master')

@section('content')

        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Gestione Utenti
        <small>gestisci gli utenti per l'accesso all Admin di OmegaCms</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="{{ route('dashboard') }}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Gestione Utenti</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Aggiungi Utente</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            @include('Admin._partials.errors')
            {!! Form::open(['route' => 'Admin.Users.store', 'method' => 'post','class' => 'form-horizontal']) !!}
                <div class="box-body">
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Username</label>

                        <div class="col-sm-10">
                            <input name="name" class="form-control" id="inputusername" placeholder="Username" type="name">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

                        <div class="col-sm-10">
                            <input name="email" class="form-control" id="inputEmail3" placeholder="Email" type="email">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Password</label>

                        <div class="col-sm-10">
                            <input name="password" class="form-control" id="inputPassword3" placeholder="Password" type="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword3" class="col-sm-2 control-label">Conferma Password</label>

                        <div class="col-sm-10">
                            <input name="password_confirmation" class="form-control" id="inputPassword3" placeholder="Password" type="password_confirmation">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <button type="submit" class="btn btn-info pull-right">Crea</button>
                </div>
                <!-- /.box-footer -->
            {!! Form::close() !!}
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Lista utenti</h3>

            <div class="box-tools pull-right">
                <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                    <i class="fa fa-minus"></i></button>
                <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                    <i class="fa fa-times"></i></button>
            </div>
        </div>
        <div class="box-body">
            @include('Admin._partials.flash')
            <hr>
            <div class="table-responsive">
                <table class="table no-margin">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>USERNAME</td>
                        <td>EMAIL</td>
                        <td>PASSWORD</td>
                        <td>Options</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                    <tr>
                        <td>{{ $user->id }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ $user->email }}</td>
                        <td>{{ $user->password }}</td>
                        <td>
                            <div class="btn-group">
                                <a><button data-original-title="Edit" type="button" class="btn btn-info btn-sm"
                                        data-widget="edita" data-toggle="tooltip" title="">
                                    <i class="fa fa-edit"></i></button></a>
                                <a href="{{ route('Admin.Users.destroy',['id' => $user->id]) }}"><button data-original-title="Remove" type="button" class="btn btn-info btn-sm"
                                        data-widget="cancella" data-toggle="tooltip" title="">
                                    <i class="fa fa-remove"></i></button></a>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->
    </section>

@endsection