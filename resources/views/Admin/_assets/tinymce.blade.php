<script type="text/javascript" src="{{ asset('Admincms/js/tinymce/tinymce.min.js') }}"></script>
<script type="text/javascript">
    tinymce.init(
            {!! json_encode(config('tinymce.params')) !!}
    );
</script>