@extends('Admin.admin_master')

@section('content')

        <!-- Content Header (Page header) -->
<section class="content-header">
    <h1>
        Pagina News
        <small>Lista delle News</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="/Admin/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="/Admin/Articles">News</a></li>
    </ol>
</section>
<section class="container">
    @include('vendor.flash.message')
</section>

<!-- Main content -->
<section class="content">

    <!-- Default box -->
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Lista news</h3>

            <div class="box-tools pull-right">
                <a href="{{ route('Admin.Article.create') }}"><button type="button" class="btn btn-primary">Crea nuova News</button></a>
            </div>
        </div>
        <div class="box-body">
            <div class="table-responsive">
                <table class="table no-margin">
                    <thead>
                    <tr>
                        <td>ID</td>
                        <td>Image</td>
                        <td>titolo</td>
                        <td>Testo</td>
                        <td>Options</td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($articles as $article)
                        <tr>
                            <td>{{ $category->id }}</td>
                            <td><img src="{{ $article->getFirstMediaUrl('images', 'adminThumb') }}"></td>
                            <td>{{ $article->title }}</td>
                            <td>{{ $article->body }}</td>
                            <td>
                                <div class="btn-group">
                                    <a href="{{ route('Admin.Article.edit', ['article' => $article->id]) }}">
                                        <button data-original-title="edit" type="button" class="btn btn-info btn-sm" data-widget="edita" data-toggle="tooltip" title="">
                                            <i class="fa fa-edit"></i></button></a>
                                    <a href="{{ route('Admin.Article.destroy',['carticle' => $article->id]) }}">
                                        <button data-original-title="Remove" type="button" class="btn btn-info btn-sm" data-widget="cancella" data-toggle="tooltip" title="">
                                            <i class="fa fa-remove"></i></button></a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
            Footer
        </div>
        <!-- /.box-footer-->
    </div>
    <!-- /.box -->

</section>
<!-- /.content -->

@endsection
