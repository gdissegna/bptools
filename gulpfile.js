var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.copy('template/vendor/font-awesome/css/font-awesome.min.css', 'resources/assets/css/font-awesome.css');
    mix.copy('template/vendor/bootstrap/css/bootstrap.min.css','resources/assets/css/bootstrap.css');
    mix.copy('template/vendor/owl.carousel/assets/owl.carousel.css','resources/assets/css/owl.carousel.css');
    mix.copy('template/vendor/owl.carousel/assets/owl.theme.default.min.css','resources/assets/css/owl.theme.default.min.css');
    mix.copy('template/vendor/simple-line-icons/css/simple-line-icons.min.css','resources/assets/css/simple-line-icons.min.css');
    mix.copy('template/vendor/magnific-popup/magnific-popup.min.css','resources/assets/css/magnific-popup.min.css');
    mix.copy('template/css/theme.css','resources/assets/css/theme.css');
    mix.copy('template/css/theme-elements.css','resources/assets/css/theme-elements.css');
    mix.copy('template/css/theme-blog.css','resources/assets/css/theme-blog.css');
    mix.copy('template/css/theme-shop.css','resources/assets/css/theme-shop.css');
    mix.copy('template/css/theme-animate.css','resources/assets/css/theme-animate.css');
    mix.copy('template/vendor/rs-plugin/css/settings.css','resources/assets/css/settings.css');
    mix.copy('template/vendor/rs-plugin/css/layers.css','resources/assets/css/layers.css');
    mix.copy('template/vendor/rs-plugin/css/navigation.css','resources/assets/css/navigation.css');
    mix.copy('template/vendor/circle-flip-slideshow/css/component.css','resources/assets/css/component.css');
    mix.copy('template/css/skins/default.css','resources/assets/css/default.css');
    mix.copy('template/css/custom.css','resources/assets/css/custom.css');
});

elixir(function(mix) {
    mix.copy('template/vendor/jquery/jquery.min.js', 'resources/assets/js/jquery.min.js');
    mix.copy('template/vendor/jquery.appear/jquery.appear.min.js', 'resources/assets/js/jquery.appear.min.js');
    mix.copy('template/vendor/jquery.easing/jquery.easing.min.js', 'resources/assets/js/jquery.easing.min.js');
    mix.copy('template/vendor/jquery-cookie/jquery-cookie.min.js', 'resources/assets/js/jquery-cookie.min.js');
    mix.copy('template/vendor/bootstrap/js/bootstrap.min.js', 'resources/assets/js/bootstrap.min.js');
    mix.copy('template/vendor/common/common.min.js', 'resources/assets/js/common.min.js');
    mix.copy('template/vendor/jquery.validation/jquery.validation.min.js', 'resources/assets/js/jquery.validation.min.js');
    mix.copy('template/vendor/jquery.stellar/jquery.stellar.min.js', 'resources/assets/js/jquery.stellar.min.js');
    mix.copy('template/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js', 'resources/assets/js/jquery.easy-pie-chart.min.js');
    mix.copy('template/vendor/jquery.gmap/jquery.gmap.min.js', 'resources/assets/js/jquery.gmap.min.js');
    mix.copy('template/vendor/jquery.lazyload/jquery.lazyload.min.js', 'resources/assets/js/jquery.lazyload.min.js');
    mix.copy('template/vendor/isotope/jquery.isotope.min.js', 'resources/assets/js/jquery.isotope.min.js');
    mix.copy('template/vendor/owl.carousel/owl.carousel.min.js', 'resources/assets/js/owl.carousel.min.js');
    mix.copy('template/vendor/magnific-popup/jquery.magnific-popup.min.js', 'resources/assets/js/jquery.magnific-popup.min.js');
    mix.copy('template/vendor/vide/vide.min.js', 'resources/assets/js/vide.min.js');
    mix.copy('template/js/theme.js', 'resources/assets/js/theme.js');
    mix.copy('template/vendor/rs-plugin/js/jquery.themepunch.tools.min.js', 'resources/assets/js/jquery.themepunch.tools.min.js');
    mix.copy('template/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js', 'resources/assets/js/jquery.themepunch.revolution.min.js');
    mix.copy('template/vendor/circle-flip-slideshow/js/jquery.flipshow.min.js', 'resources/assets/js/jquery.flipshow.min.js');
    mix.copy('template/js/views/view.home.js', 'resources/assets/js/view.home.js');
    mix.copy('template/js/custom.js', 'resources/assets/js/custom.js');
    mix.copy('template/js/theme.init.js', 'resources/assets/js/theme.init.js');
});

elixir(function(mix) {
    mix.styles([
        'font-awesome.css',
        'bootstrap.css',
        'owl.carousel.css',
        'owl.theme.default.min.css',
        'simple-line-icons.min.css',
        'magnific-popup.min.css',
        'theme.css',
        'theme-elements.css',
        'theme-blog.css',
        'theme-shop.css',
        'theme-animate.css',
        'settings.css',
        'layers.css',
        'navigation.css',
        'component.css',
        'default.css',
        'custom.css',

    ], 'public/Front/css/all.css');
});

elixir(function(mix) {
    mix.scripts([
        'jquery.min.js',
        'jquery.appear.min.js',
        'jquery.easing.min.js',
        'jquery-cookie.min.js',
        'bootstrap.min.js',
        'common.min.js',
        'jquery.validation.min.js',
        'jquery.stellar.min.js',
        'jquery.easy-pie-chart.min.js',
        'jquery.gmap.min.js',
        'jquery.lazyload.min.js',
        'jquery.isotope.min.js',
        'owl.carousel.min.js',
        'jquery.magnific-popup.min.js',
        'vide.min.js',
        'theme.js',
        'jquery.themepunch.tools.min.js',
        'jquery.themepunch.revolution.min.js',
        'jquery.flipshow.min.js',
        'view.home.js',
        'custom.js',
        'theme.init.js',

    ],'public/Front/js/all.js');
});

