<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->string('locale')->index();
            $table->text('slug');
            $table->string('name');
            $table->text('description');
            $table->text('note');
            $table->string('meta_name')->nullable();
            $table->string('meta_description')->nullable();
            $table->integer('product_id')->unsigned();;
            $table->foreign('product_id')->references('id')->on('products')->onDelete('Cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product_translations');
    }
}
