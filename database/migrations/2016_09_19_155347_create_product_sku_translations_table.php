<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSkuTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_sku_translations', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('locale')->index();
            $table->string('name',255)->nullable();
            $table->integer('product_sku_id')->unsigned();
            $table->foreign('product_sku_id')->references('id')->on('product_skus')->onDelete('Cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_sku_translations');
    }
}
