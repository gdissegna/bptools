<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductSkuDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_sku_datas', function (Blueprint $table) {
            $table->increments('id');
	        $table->string('codice_articolo');
	        $table->string('diametro')->nullable();
	        $table->string('diametro_mm')->nullable();
	        $table->string('diametro_f')->nullable();
	        $table->string('sp')->nullable();
	        $table->string('sp_c')->nullable();
	        $table->string('tuh_mm')->nullable();
	        $table->string('htc')->nullable();
	        $table->string('asr_n')->nullable();
	        $table->string('dxf')->nullable();
	        $table->string('l')->nullable();
	        $table->string('lu')->nullable();
	        $table->string('lt')->nullable();
	        $table->string('p')->nullable();
	        $table->string('r')->nullable();
	        $table->string('spessore')->nullable();
	        $table->string('profondità')->nullable();
	        $table->string('profondita_taglio')->nullable();
	        $table->string('v')->nullable();
	        $table->string('z')->nullable();
	        $table->string('w1')->nullable();
	        $table->string('w2')->nullable();
	        $table->string('w')->nullable();
	        $table->string('t')->nullable();
	        $table->string('h1')->nullable();
	        $table->string('h2')->nullable();
	        $table->string('dim')->nullable();
	        $table->string('hs')->nullable();
	        $table->string('hw')->nullable();
	        $table->string('acciaio')->nullable();
	        $table->string('lega_leggera')->nullable();
	        $table->string('a_z=12')->nullable();
	        $table->string('b_z=16')->nullable();
	        $table->string('c_z=16')->nullable();
	        $table->string('prof')->nullable();
	        $table->string('profilo')->nullable();
	        $table->string('t_max')->nullable();
	        $table->string('tipo')->nullable();
	        $table->string('n_b')->nullable();
	        $table->string('n_d')->nullable();
	        $table->string('tipo_a')->nullable();
	        $table->string('tipo_b')->nullable();
	        $table->string('cod_rh')->nullable();
	        $table->string('cod_lh')->nullable();
	        $table->string('b')->nullable();
	        $table->string('c')->nullable();
	        $table->string('type')->nullable();
	        $table->string('wretches')->nullable();
	        $table->string('thread')->nullable();
	        $table->string('pinza')->nullable();
	        $table->string('z_2')->nullable();
	        $table->string('z_3')->nullable();
	        $table->string('z_4')->nullable();
	        $table->string('z_6')->nullable();
	        $table->string('z_8')->nullable();
	        $table->string('z_12')->nullable();
	        $table->string('z_24')->nullable();
	        $table->string('z_4+4')->nullable();
	        $table->string('z_2+2')->nullable();
	        $table->string('z_3+3')->nullable();
	        $table->string('z_3+3+3')->nullable();
	        $table->string('z_4+4+4')->nullable();
	        $table->string('z_4+4r')->nullable();
	        $table->string('z_4+v8')->nullable();
	        $table->string('z_6+3')->nullable();
	        $table->string('hw_z_4+4')->nullable();
            $table->integer('product_sku_id')->unsigned();
            $table->foreign('product_sku_id')->references('id')->on('product_skus')->onDelete('Cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_sku_datas');
    }
}


/*
 *
 *
 */