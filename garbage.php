<?php
/*TP implementation*/
//Including Tracking
echo "<script type=\"text/javascript\" src=\"https://tracking.trovaprezzi.it/javascripts/tracking.min.js\"></script>";
//Printing Script
echo "<script type=\"text/javascript\">";
echo "window._tt = window._tt || [];";
echo "window._tt.push({ event: \"setAccount\", id: '" . $this->session->data['TPtpi']['chiaveMerchant'] . "' });";
echo "window._tt.push({ event: \"setOrderId\", order_id: " . $this->session->data['TPtpi']['orderid'] . " });";
echo "window._tt.push({ event: \"setEmail\", email: '" . $this->session->data['TPtpi']['email'] . "' });";
foreach ($this->session->data['TPtpi']['prods'] as $p) {
    echo "window._tt.push({ event: \"addItem\", sku: '" . $p['model'] . "', product_name: '" . $p['name'] . "' });";
}
echo "window._tt.push({ event: \"setAmount\", amount: '" . $this->session->data['TPtpi']['amount'] . "' });";
echo "window._tt.push({ event: \"orderSubmit\"});";
echo "</script>"; ?>