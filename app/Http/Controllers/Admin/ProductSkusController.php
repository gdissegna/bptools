<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CreateSkuRequest;
use App\Http\Requests\Admin\EditSkuRequest;
use \App\Repository\ProductSkuRepository;

class ProductSkusController extends Controller {

	protected $productSku;

	public function __construct(ProductSkuRepository $productSku) {
		$this->productSku = $productSku;
	}

	public function index($product_id) {
		$allskus = $this->productSku->getByparentId($product_id);

		return view('Admin.products.productskus')->with('allskus', $allskus)
			->with('product_id',$product_id);
	}

	public function create($product_id) {


		return view('Admin.products.productskuscreate')->with('product_id', $product_id);
	}

	public function store(CreateSkuRequest $request, $product_id) {
		$this->productSku->create($request,$product_id);

		return redirect()->route('Admin.ProductSku.index',['Product', $product_id]);
	}

	public function edit($product_id, $sku_id) {
		$skutable = $this->productSku->find($sku_id);

		return view('Admin.products.productskusedit')->with('skutable', $skutable)
		                                             ->with('product_id', $product_id)
		                                             ->with('sku_id', $sku_id);
	}

	public function update(EditSkuRequest $request ,$sku_id) {
		$this->productSku->edit($request,$sku_id);
	}

	public function delete($product_id, $sku_id) {

		$this->productSku->delete($sku_id);

		return redirect('Admin.ProductSku.index', ['product' => $product_id]);
	}

}
