<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\CreateSkuDataRequest;
use App\Http\Requests\Admin\EditSkudataRequest;
use App\Repository\ProductSkuDataRepository;

class ProductSkuDatasController extends Controller
{
	protected $skudata;

	/**
	 * ProductSkuDatasController constructor.
	 *
	 * @param ProductSkudataRepository $skudata
	 */
	 public function __construct(ProductSkudataRepository $skudata) {
		$this->skudata = $skudata;
	}

	public function index($product_id,$sku_id) {
		$allskudata = $this->skudata->allrecords($sku_id);

		return view('Admin.products.productskudatatable')->with('allskudata', $allskudata)
		                                                       ->with('product_id', $product_id)
															   ->with('sku_id', $sku_id);
	}

	public function create($product_id,$sku_id) {

		return view('Admin.products.productskudatatablecreate')
			->with('product_id', $product_id)
			->with('sku_id', $sku_id);
	}

	public function store($product_id,$sku_id, CreateSkuDataRequest $request) {
		try{
			$this->skudata->create($request,$sku_id);
		} catch (\Exception $e) {
			flash('errore nel inserire nuovo record','error');
			return redirect()->back()->withInput();
		}
		flash('aggiunto nuovo record alla tabella','success');

		return redirect()->route('Admin.ProductSkudata.index', ['Product' => $product_id, 'sku' => $sku_id]);
	}

	/**
	 * @param $product_id
	 * @param $sku_id
	 * @param $skudata_id
	 * @param EditSkudataRequest $request
	 */
	public function edit($product_id, $sku_id, $skudata_id) {
		$editskudata = $this->skudata->find($skudata_id);

		return view('Admin.products.productskusedit',['Product' => $product_id, 'sku' => $sku_id, 'skudata' => $skudata_id])
			->with('editskudata',$editskudata);
	}

	public function update($product_id,$sku_id,$skudata_id, EditSkudataRequest $request) {
		try {
			$this->skudata->edit($request, $skudata_id);
		} catch (\Exception $e) {
			flash('errore nel aggiornamento del record record','error');
			return view('Admin.products.productskudatatable');
		}
		flash('modificad record tabella completato','success');

		return redirect()->route('Admin.ProductSkudata.index', ['Product' => $product_id, 'sku' => $sku_id]);
	}

	public function delete($product_id, $sku_id , $skudata_id) {

		$this->skudata->delete($skudata_id);

		return redirect()->route('Admin.ProductSkudata.index', ['Product' => $product_id, 'sku' => $sku_id]);
	}
}
