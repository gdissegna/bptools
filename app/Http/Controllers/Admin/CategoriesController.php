<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Requests\CategoryImageUpdateRequest;
use Laracasts\Flash\Flash;
use App\Http\Controllers\Controller;
use App\Repository\CategoryRepository;
use App\Http\Requests\Admin\CategoryRequest;
use App\Http\Requests\Admin\CreateCategoryImageRequest;



class CategoriesController extends Controller
{

    protected $category;

    /**
     * CategoriesController constructor.
     * @param $category
     */
    public function __construct(CategoryRepository $category)
    {
        $this->category = $category;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = $this->category->All();

        return view('Admin.categories.categories')
            ->with('categories', $categories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $parent_categories = $this->category->allParents();
        return view('Admin.categories.create')->with('parent_categories', $parent_categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Admin\CategoryRequest  $request
     *
     * @return \Illuminate\Contracts\View\View
     */
    public function store(CategoryRequest $request)
    {
        $this->category->create($request);

	    $parent_categories = $this->category->allParents();

        Flash::success('La categoria è stata creata correttamente');

	    return view('Admin.categories.create')->with('parent_categories', $parent_categories);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd('you are here');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $edit_category = $this->category->find($id);

        return view('Admin.categories.edit')->with('edit_category', $edit_category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Admin\CategoryRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryRequest $request, $id)
    {
        $category = $this->category->update($request, $id);

        \Flash::success('Categoria modificata correttamente');

        return redirect()->route('Admin.Category.index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $this->category->delete($id);

        flash()->success('La categoria è stata eliminata dal database.');

        return redirect()->route('Admin.Category.index');
    }

    /**
     * Display all the images and files associate to the model
     * @param $id
     * @return mixed
     */
    public function imagesIndex($id)
    {
        $category_images = $this->category->find($id);

        return view('Admin.categories.images')->with('category_images', $category_images);
    }

    /**
     * Associates a new file or image to the Model
     * @param CreateCategoryImageRequest $request
     * @param $id
     * @return mixed
     */
    public function imagesStore(CreateCategoryImageRequest $request,$id)
    {
        $this->category->addMedia($request,$id);
        \Flash::success('Immagine\file aggiunto con successo');
        return redirect()->route('Admin.CategoryImages.index',[$id]);
    }

    public function imagesEdit($category_id,$media_id)
    {
        $single_media = $this->category->findMedia($category_id,$media_id);

        return view('admin.categories.images_edit')->with('single_media', $single_media);
    }

    public function imagesUpdate(CategoryImageUpdateRequest $request,$category_id,$media_id)
    {
        $media_updated = $this->category->updateMedia($request, $category_id, $media_id);
        \Flash::success('Immagine Modificata con successo');
        return redirect()->route('Admin.CategoryImages.index');
    }

    /**
     * @param $category_id
     * @param $media_id
     * @return mixed
     */
    public function imagesDelete($category_id,$media_id)
    {
       $this->category->deleteMedia($category_id,$media_id);
        \Flash::success('Immagine\file cancellato con successo');

        return redirect()->route('Admin.CategoryImages.index',[$category_id]);
    }

}
