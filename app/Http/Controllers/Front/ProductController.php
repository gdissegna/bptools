<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Repository\ProductRepository;

class ProductController extends Controller
{
	/**
	 * ProductController constructor.
	 */
	public function __construct(ProductRepository $productRepository)
	{
		$this->productRepository = $productRepository;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index($category_slug, $product_slug)
	{
		$product = $this->productRepository->findBySlug($product_slug);



		return view('Front.product')->with('product', $product);
	}

}
