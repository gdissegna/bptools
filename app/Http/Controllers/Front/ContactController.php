<?php

namespace App\Http\Controllers\Front;

use App\Http\Requests\ContactFormRequest;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    /**
     * ContactController constructor.
     */
    public function __construct()
    {

    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('Front.contact');
    }

    public function ContactInfoEmail(ContactFormRequest $contactFormRequest)
    {
        $this->dispatch(new SendContactEmailJob($contactFormRequest));
        Flash::success('Il Form è stato inviato Correttamente');
        redirect()->route('contact');
    }
}
