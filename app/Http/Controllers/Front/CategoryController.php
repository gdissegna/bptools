<?php

namespace App\Http\Controllers\Front;

use App\Repository\CategoryRepository;
use App\Http\Controllers\Controller;

class CategoryController extends Controller
{
    private $category;

    /**
     * CategoryController constructor.
     */
    public function __construct(CategoryRepository $categoryRepository)
    {
        $this->category = $categoryRepository;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($category_slug)
    {
    	$cat_products = $this->category->findBySlug($category_slug);

        return view('Front.category')->with('cat_products', $cat_products);
    }

    public function allCategories() {
	    $all_categories = $this->category->allParents();

	    return view('Front.listCategories')->with('all_categories', $all_categories);
    }

}
