<?php

namespace App\Http\Controllers\Front;

use Illuminate\Session;
use App\Http\Requests\CartAddRequest;
use App\Http\Requests\CartRemoveRequest;
use App\Http\Requests\CartFormRequest
use App\Http\Controllers\Controller;

class CartController extends Controller {

	public function __construct() {
	}

	public function index() {



		return view('Front.cart')
	}

	public function addToCart(CartAddRequest $request) {

		dd($request);
		Session::push('cart', $id);

		Session::flash('product_added', trans('custom.prodotto_aggiunto'));

		return redirect()->back();
	}

	public function removeFromCart(CartRemoveRequest $request) {

	}

	public function CartEmail(CartFormRequest $request) {

	}

}
