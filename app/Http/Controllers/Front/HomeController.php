<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Repository\ArticleRepository;
use App\Repository\CategoryRepository;
use App\Repository\ProductRepository;

class HomeController extends Controller
{
    public $categories;
    public $articles;
    public $products;
    public $sliders;

    public function __construct(CategoryRepository $categories, ArticleRepository $articles, ProductRepository $products)
    {
        $this->categories = $categories;
        $this->articles = $articles;
        $this->products = $products;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    	$menu_categories = $this->categories->allParents();
        $new_products = $this->products->findBy('is_new', true);
        $home_products = $this->products->findBy('is_home', true);
        $home_articles = $this->articles->lastest();

        return view('Front.home')->with('menu_categories', $menu_categories)
	        ->with('new_products', $new_products)
            ->with('home_products', $home_products)
            ->with('home_articles', $home_articles);
    }
}
