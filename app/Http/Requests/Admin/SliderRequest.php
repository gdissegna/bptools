<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;
use LaravelLocalization;

class SliderRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'image' => 'required|image',
        ];

        foreach(LaravelLocalization::getSupportedLanguagesKeys() as $lang)
        {
            $rules['title_'. $lang] = 'required';
            $rules['text_'. $lang] = 'required';
        }

        return $rules;
    }
}
