<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;

class EditArticleRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'image' => 'required|image',
        ];

        foreach(config('app.locales') as $lang => $name)
        {
            $rules['title_'. $lang] = 'required';
            $rules['body_'. $lang] = 'required';
        }

        return $rules;
    }
}
