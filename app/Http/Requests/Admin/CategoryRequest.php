<?php

namespace App\Http\Requests\Admin;

use App\Http\Requests\Request;


class CategoryRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'image' => 'required|image',
        ];

        foreach(config('app.locales') as $lang => $name)
        {
            $rules['name_'. $lang] = 'required';
            $rules['description_'. $lang] = 'required';
        }

        return $rules;
    }
}
