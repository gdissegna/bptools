<?php

namespace App\Http\ViewComposers;

use App\Repository\SliderRepository;
use Illuminate\Contracts\View\View;

class SliderComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $sliders;

    /**
     * Create a new profile composer.
     *
     * @param SliderRepository $sliders
     */
    public function __construct(SliderRepository $sliders)
    {
        $this->sliders = $sliders;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

        $view->with('sliders', $this->sliders->all());
    }
}