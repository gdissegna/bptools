<?php

namespace App\Http\ViewComposers;

use App\Repository\CategoryRepository;
use Illuminate\Contracts\View\View;

class MenuCategoriesComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $categories;

    /**
     * Create a new profile composer.
     *
     * @param SliderRepository $sliders
     */
    public function __construct(CategoryRepository $categories)
    {
        $this->categories = $categories;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {

        $view->with('menu_categories', $this->categories->all());
    }
}