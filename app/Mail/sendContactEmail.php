<?php

namespace App\Mail;

use App\Http\Requests\Front\ContactFormRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Requests\Front\CartFormRequest

class sendContactEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $contact;

	/**
	 * Create a new message instance.
	 *
	 * @param ContactFormRequest $contact
	 */
    public function __construct(ContactFormRequest $contact)
    {
        $this->contact = $contact
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('view.name');
    }
}
