<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Http\Requests\Front\CartFormRequest;

class sendCartEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected $cart

	/**
	 * Create a new message instance.
	 *
	 * @param CartFormRequest $cart
	 */
    public function __construct($cart, CartFormRequest $cartform)
    {
        $this->cart = $cart;
	    $this->cartform = $cartform;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('Mail.Cart');
    }
}
