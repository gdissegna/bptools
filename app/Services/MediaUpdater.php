<?php

namespace App\Services;

use Spatie\MediaLibrary\FileAdder\FileAdder;

class MediaUpdater extends FileAdder
{
    /**
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function getSubject()
    {
        return $this->subject;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getPathToFile()
    {
        return $this->pathToFile;
    }

    public function getFilename()
    {
        return $this->fileName;
    }


    public function updateMediaData()
    {
        if (!$this->subject->exists) {
            throw FileCannotBeAdded::modelDoesNotExist($this->subject);
        }
        if (!is_file($this->pathToFile)) {
            throw FileCannotBeAdded::fileDoesNotExist($this->pathToFile);
        }
        if (filesize($this->pathToFile) > config('laravel-medialibrary.max_file_size')) {
            throw FileCannotBeAdded::fileIsTooBig($this->pathToFile);
        }

        $media = $this->getSubject();

        $media->name = $this->mediaName;
        $media->file_name = $this->fileName;
        $media->size = filesize($this->pathToFile);
        $media->custom_properties = $this->customProperties;
        $media->manipulations = [];
        $media->fill($this->properties);
        $media->save();

        $this->setSubject($media);

        return $this;
    }

}