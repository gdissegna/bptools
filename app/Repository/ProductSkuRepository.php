<?php

namespace App\Repository;

use App\Http\Requests\Admin\CreateSkuRequest;
use \App\Models\ProductSku;
use App\Models\ProductSkuData;

class ProductSkuRepository
{
	protected $productSku;
	protected $productSkuData;

    public function __construct(ProductSku $productSku,ProductSkuData $productSkuData)
    {
		$this->productSku = $productSku;
	    $this->productSkuData = $productSkuData;
    }

    public function all() {
	    return $this->productSku->with('SkuData')->orderBy('updated_at', 'desc')->get();
    }

	public function getByparentId($product_id) {

    	return $this->productSku->where('product_id', $product_id)->get();
	}

	public function paginate( $perPage = 10 ) {
		return $this->productSku->with('SkuData')
			->orderBy('updated_at', 'desc')
			->paginate($perPage);
	}

	public function create( CreateSkuRequest $request, $product_id) {
		$newsku = new ProductSku();

		$newsku->columns = $request['colonne'];
		foreach (config('app.locales') as $locale => $name) {
			$newsku->translateOrNew($locale)->name = $request['name_' . $locale];
		}
		$newsku->product_id = $product_id;
		$newsku->save();
	}

	public function update( Request $data, $product_id, $sku_id ) {
		$editsku = $this->productSku->find($sku_id);

		foreach (config('app.locales') as $locale => $name) {
			$newsku->translate($locale)->name = $data->has('name_'. $locale) ? $data['name_'. $locale] : $updateProduct->translate($locale)->name ;
		}

		$editsku->save();

	}

	public function delete( $id ) {
		$deleteSku = $this->productSku->find($id);
		return $deleteSku->delete();
	}

	public function find( $id ) {
		$singlesku = $this->productSku->find($id);
		return $singlesku;
	}

	public function findBy($field, $value) {
		$skus = $this->productSku->where($field,$value)->get();
		return $skus;
	}

}