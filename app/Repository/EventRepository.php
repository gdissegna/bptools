<?php
namespace App\Repository;

use App\Http\Requests\Request;
use App\Models\Event;

class EventRepository
{
    protected $event;

    /**
     * EventRepository constructor.
     */
    public function __construct(Event $event)
    {
        $this->event = $event;
    }

    public function all()
    {
        return $this->event->orderBy('updated_at','desc')->all();
    }

    public function find($id)
    {
        return $this->event->find($id);
    }

    public function findBy($field,$value)
    {
        return $this->event->orderBy('updated_at','desc')->where($field,$value)->get();
    }

    public function paginate($perPage = 15)
    {
        return $this->event->paginate($perPage);
    }

    public function create(Request $request)
    {
    }

    public function update(Request $request, $id)
    {
    }

    public function delete($id)
    {
    }

    public function addMerdia()
    {
    }

    public function updateMedia()
    {
    }

    public function deleteMedia()
    {
    }
}