<?php


namespace App\Repository;

use App;
use App\Models\Translations\ProductTranslation;
use App\Repository\RepositoryInterface;
use App\Http\Requests\Request;
use App\Models\Product;

/**
 * Class ProductRepository
 * @package App\Repository
 */
class ProductRepository implements RepositoryInterface
{
	protected $product;
	protected $translations;

    /**
     * ProductRepository constructor.
     * @param Product $product
     */
    public function __construct(Product $product,ProductTranslation $productTranslation)
    {
        $this->product = $product;
        $this->translations = $productTranslation;
    }

    /**
     * @return mixed
     */
    public function all()
    {
        return $this->product->with('media')->orderBy('updated_at', 'desc')->get();
    }

    /**
     * @param $perPage
     * @return mixed
     */
    public function paginate($perPage = 20)
    {
        return $this->product->with('media')->orderBy('updated_at', 'desc')->paginate($perPage);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function create(Request $request)
    {
        $newProduct = new Product();

        foreach (config('app.locales') as $locale => $name) {
            $newProduct->translateOrNew($locale)->name = $request['name_' . $locale];
            $newProduct->translateOrNew($locale)->description = $request['description_' . $locale];
	        $newProduct->translateOrNew($locale)->note = $request['note_' . $locale];
        }

	    if($request->exists('code')){
		    $newProduct->code = $request['code'];
	    }
	    if($request->exists('is_new')){
		    $newProduct->is_new = 1;
	    } else {
		    $newProduct->is_new = 0;
	    }
	    if($request->exists('is_home')){
		    $newProduct->is_home = 1;
	    } else {
		    $newProduct->is_home = 0;
	    }

	    $newProduct->category_id = $request['parent'];

        $newProduct->save();

        $newProduct->addMedia($request->file('image'))
	                ->withCustomProperties(['testoimg' => 'primary image'])
	                ->withCustomProperties(['Note_it' => 'primary image'])
	                ->withCustomProperties(['Note_en' => 'primary image'])
                    ->toCollection('images');
        $newProduct->save();

        return $newProduct;
    }

    /**
     * @param Request $data
     * @param $id
     * @return mixed
     */
    public function update(Request $data, $id)
    {
	    $updateProduct = $this->product->find($id);

	    foreach (config('app.locales') as $locale => $name) {
		    $updateProduct->translate($locale)->name = $data->has('name_'. $locale) ? $data['name_'. $locale] : $updateProduct->translate($locale)->name ;
		    $updateProduct->translate($locale)->description = $data->has('description_'. $locale) ? $data['description_'. $locale] : $updateProduct->translate($locale)->description;
		    $updateProduct->translate($locale)->meta_title = $data->has('meta_title_'. $locale) ? $data['meta_title_'. $locale ] : $updateProduct->translate($locale)->meta_title;
		    $updateProduct->translate($locale)->meta_description = $data->has('name_'. $locale) ? $data['meta_description_'. $locale ] : $updateProduct->translate($locale)->meta_description;
	    }

	    if($data->exists('code')){
		    $updateProduct->code = $data['code'];
	    }
	    if($data->exists('is_new')){
		    $updateProduct->is_new = 1;
	    } else {
		    $updateProduct->is_new = 0;
	    }
	    if($data->exists('is_home')){
		    $updateProduct->is_home = 1;
	    } else {
		    $updateProduct->is_home = 0;
	    }

	    if($data->exists('numero_pezzi')){
		    $updateProduct->numero_pezzi = $data['numero_pezzi'];
	    }

	    $updateProduct->category_id = $data['parent'];

	    return $updateProduct->save();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function delete($id)
    {
        $prod_delete = $this->find($id);

        return $prod_delete->delete();
    }

    /**
     * @param $id
     * @return mixed
     */
    public function find($id)
    {
        return $this->product->findOrFail($id);
    }

    /**
     * @param $field
     * @param $value
     * @return mixed
     */
    public function findBy($field, $value)
    {
        return $this->product->where($field, $value)->get();
    }

	public function findBySlug($slug)
	{
		$this->translations = new ProductTranslation();

		$translation = $this->translations->findBySlug($slug);

		if ( ! $translation)
		{
			return App::abort(404);
		}

		return $this->product->with('media')->where('id', $translation->product_id)->first();
	}

    public function addMedia(Request $request, $id)
    {
	    $product_media = $this->product->findOrFail($id);

        $product_media->addMedia($request->file('image'))
	                  ->withCustomProperties(['testoimg' => $request['alttext']])
	                  ->withCustomProperties(['Note_it' => $request['note_en']])
	                  ->withCustomProperties(['Note_en' => $request['note_it']])
                      ->toCollection($request['collection']);
    }


    public function deleteMedia($product_id, $media_id)
    {
        $parent_product = $this->product->find($product_id);

        $single_media = $parent_product->getMedia()->where('id', (int)$media_id)->first();

        $single_media->delete();
    }

}