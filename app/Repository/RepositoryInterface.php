<?php

namespace App\Repository;


use App\Http\Requests\Request;

interface RepositoryInterface
{
    public function all();

    public function paginate($perPage);

    public function create(Request $data);

    public function update(Request $data, $id);

    public function delete($id);

    public function find($id);

    public function findBy($field, $value);
}