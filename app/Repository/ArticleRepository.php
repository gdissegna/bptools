<?php

namespace App\Repository;

use App\Http\Requests\Request;
use App\Models\Article;
use Log;

class ArticleRepository implements RepositoryInterface
{
    public $article;
    public $perPage;

    /**
     * @param Article $article
     */
    public function __construct(Article $article)
    {
        $this->article = $article;
    }

    public function all()
    {
        return $this->article->with('media')->orderBy('updated_at', 'desc')->get();
    }

    public function paginate($perPage)
    {
        return $this->article->with('media')
            ->orderBy('updated_at', 'desc')
            ->paginate($perPage);
    }

    /**
     * @return Article
     */
    public function lastest()
    {
        return $this->article->with('media')
            ->orderBy('updated_at','desc')
            ->take(3)
            ->get();
    }

    public function create(Request $data)
    {
        $newarticle = new Article();

        foreach (config('app.locales') as $locale => $name) {
            $newarticle->translateOrNew($locale)->title = $request['name_'. $locale];
            $newarticle->translateOrNew($locale)->body = $request['description_'. $locale];
            $newarticle->translateOrNew($locale)->meta_title = $request['meta_title'. $locale];
            $newarticle->translateOrNew($locale)->meta_description = $request['meta_description'. $locale];
        }

        $newarticle->save();

        $newarticle->addMedia($request->file('image'))->toCollection('images');
        $newarticle->save();

    }

    public function update(Request $request, $id)
    {
        $edit_article = $this->article->find($id);

        foreach (config('app.locales') as $locale => $name) {
            $edit_article->translateOrNew($locale)->title = $request->has('title_'. $locale) ?? $request['title_'. $locale];
            $edit_article->translateOrNew($locale)->body = $request->has('body_'. $locale) ?? $request['body_'. $locale];
            $edit_article->translateOrNew($locale)->meta_title = $request->has('meta_title_'. $locale) ?? $request['meta_title'. $locale];
            $edit_article->translateOrNew($locale)->meta_description = $request->has('meta_description_'. $locale) ?? $request['meta_description'. $locale];
        }

        $edit_article->save();
    }

    public function delete($id)
    {
        $todelete = $this->article->find($id);

        $todelete->delete();
    }

    public function find($id)
    {
        return $this->article->find($id);
    }

    public function findBy($field, $value)
    {
        return $this->article->where($field, $value)->get();
    }

    public function findBySlug($slug)
    {
       try {
           $trans_id = $this->article->translations()->where('slug', $slug)->first();
       }  catch (Exception $e) {
            Log::error($e->getMessage());
            flash('non e stata trovata nessuna slug corrispondente');
            redirect()->back();
       }
        return $this->article->find($trans_id);
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function addMedia(Request $request,$id)
    {
        $article_media_edit = $this->category->find($id);

        $article_media_edit->addMedia($request['image'])->toCollection($request['collection']);
    }

    public function updateMedia(Request $request, $article_id, $media_id)
    {
        $media_article = $this->article>find($category_id);
        $single_media = $media_article->getMedia()->where('id', (int)$media_id)->first();

        $this->article->updateMedia($single_media, $request->file('image'));
    }

    public function deleteMedia($article_id, $media_id)
    {
        $parent_article = $this->article->find($category_id);

        $single_media = $parent_article->getMedia()->where('id',(int) $media_id)->first();

        $single_media->delete();
    }
}