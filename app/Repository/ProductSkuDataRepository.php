<?php

namespace App\Repository;

use App\Http\Requests\Admin\CreateSkuDataRequest;
use App\Http\Requests\Admin\EditSkudataRequest;
use App\Models\ProductSku;
use App\Models\ProductSkuData;
use Config;

class ProductSkuDataRepository {

	protected $productSku;
	protected $productSkuData;

	public function __construct( ProductSku $productSku, ProductSkuData $productSkuData ) {
		$this->ProductSku     = $productSku;
		$this->productSkuData = $productSkuData;
	}

	public function all() {
		return $this->productSkuData->get();
	}

	public function allrecords( $table_id ) {
		return $this->productSkuData->where( 'product_sku_id', $table_id )->get();
	}

	public function find( $id ) {
		$skudata = $this->productSkuData->find( $id );
	}

	public function findBy( $field, $value ) {
		return $this->productSkuData->where( $field, $value );
	}

	public function create( CreateSkuDataRequest $data, $sku_id ) {
		$newskudata = new ProductSkuData();

		$newskudata->codice_articolo = $data->codice_articlo;
		$newskudata->product_sku_id  = $sku_id;

		foreach ( Config::get( 'SkuDataTables' ) as $skudata ) {
			if ( $data->has( $skudata ) ) {
				$newskudata->$skudata = $data->$skudata;

			}
		}
		$newskudata->save();
	}

	public function update( EditSkudataRequest $data, $skudata_id ) {
		$editskudata = $this->productSkuData->find( $skudata_id );
	}

	public function delete( $skudata_id ) {
		$delskudata = $this->productSkuData->find( $skudata_id );
		$delskudata->delete();
	}


}