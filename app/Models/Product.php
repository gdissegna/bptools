<?php

namespace App\Models;

use App\Models\Fundation\BaseModel;

class Product extends BaseModel
{
    public $mediaLibraryCollections = ['images', 'downloads'];
    public $translatedAttributes = ['name','slug', 'description', 'note','meta_title','meta_description'];

    public function Category()
    {
       return $this->belongsTo('App\Models\Category');
    }

    public function ProductSku()
    {
        return $this->hasMany('App\Models\ProductSku');
    }

	public function registerMediaConversions()
	{
		parent::registerMediaConversions();
		$this->addMediaConversion('micro')
		     ->setManipulations(['w' => 50, 'h' => 50, 'fit' => 'contain'])
		     ->performOnCollections('images');
		$this->addMediaConversion('Thumb')
		     ->setManipulations(['w' => 1170, 'h' => 540, 'fit' => 'contain'])
		     ->performOnCollections('images');
		$this->addMediaConversion('large')
		     ->setManipulations(['w' => 1170, 'h' => 1170, 'fit' => 'contain'])
		     ->performOnCollections('images');
	}

}
