<?php
namespace app\Models\Fundation\Traits;

use App\Services;
use Spatie\MediaLibrary\Media;
use Spatie\MediaLibrary\Filesystem;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Symfony\Component\HttpFoundation\File\UploadedFile;


trait HasMedia
{
    use HasMediaTrait;

    /**
     * @param array $attributes
     */
    protected function updateMediaLibraryFields($attributes)
    {
        if ( ! isset($this->mediaLibraryCollections)) {
            return;
        }
        foreach ($this->mediaLibraryCollections as $collectionName) {
            if (array_key_exists($collectionName, $attributes)) {
                $updatedMedia = $this->updateMedia(json_decode($attributes[$collectionName], true), $collectionName);
                foreach ($updatedMedia as $mediaItem) {
                    $mediaItem->setCustomProperty('temp', false);
                    $mediaItem->save();
                }
            }
        }
    }

    public function clearTemporaryMedia()
    {
        $this->media()->get()->each(function (Media $media) {
            if ($media->getCustomProperty('temp', false) === true) {
                $media->delete();
            }
        });
    }

    public function updateMedia(Media $media,UploadedFile $file)
    {
        // app(Filesystem::class)->removeFiles($media);
        $test = app(Services\MediaUpdater::class)->setSubject($media)->setFile($file)->updateMediaData();
        app(Filesystem::class)->add($test->getPathToFile(), $test->getSubject(), $test->getFilename());
    }

}

