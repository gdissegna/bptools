<?php

namespace App\Models\Fundation;


use Cviebrock\EloquentSluggable\Sluggable;
use Cviebrock\EloquentSluggable\SluggableScopeHelpers;

abstract class SluggableTranslation extends Translation
{
    use Sluggable,SluggableScopeHelpers;
    public $timestamps = false;
    protected $guarded = ['id'];

}