<?php
namespace app\Models\Fundation;

use Illuminate\Database\Eloquent\Model;

class Translation extends Model
{
    public $timestamps = false;
    protected $guarded = ['id'];
}