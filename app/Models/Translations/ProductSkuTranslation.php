<?php

namespace App\Models\Translations;

use App\Models\Fundation\Translation;

class ProductSkuTranslation extends Translation
{
    protected $fillable = ['name'];

    public function ProductSku()
    {
    	return $this->belongsTo('App\Models\ProductSku','product_sku_id', 'id');
    }

}
