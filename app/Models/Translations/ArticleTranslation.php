<?php

namespace App\Models\translations;

use App\Models\Fundation\SluggableTranslation;


class ArticleTranslation extends SluggableTranslation
{
    protected $fillable = ['title', 'body'];

	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'title'
			]
		];
	}
}
