<?php

namespace App\Models\translations;

use App\Models\Fundation\SluggableTranslation;

class CategoryTranslation extends SluggableTranslation
{
    protected $fillable = ['name', 'description'];

	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'name'
			]
		];
	}


    public function Category()
    {
        return $this->belongsTo('category','category_id', 'id');
    }
}
