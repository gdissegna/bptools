<?php

namespace App\Models\translations;


use App\Models\Fundation\SluggableTranslation;

class ProductTranslation extends SluggableTranslation
{
   protected $fillable = ['name','slug','description'];

	/**
	 * Return the sluggable configuration array for this model.
	 *
	 * @return array
	 */
	public function sluggable()
	{
		return [
			'slug' => [
				'source' => 'name'
			]
		];
	}

    public function Product()
    {
        return $this->belongsTo('App\Models\Product', 'product_id', 'id');
    }
}