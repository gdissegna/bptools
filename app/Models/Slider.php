<?php

namespace App\Models;

use App\Models\Fundation\BaseModel;

class Slider extends BaseModel
{
    protected $fillable = [];

    public $mediaLibraryCollections = ['images'];
    public $translatedAttributes = ['title', 'text'];
    public $translationModel = 'App\Models\translations\SliderTranslation';

}
