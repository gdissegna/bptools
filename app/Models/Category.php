<?php

namespace App\Models;

use App\Models\Fundation\BaseModel;


class Category extends BaseModel
{
    public $mediaLibraryCollections = ['images', 'downloads'];
    public $translatedAttributes = ['name','slug','description','meta_title','meta_description'];

    public function Children()
    {
       return $this->hasMany('App\Models\Category', 'id');
    }

    public function Parent()
    {
        return $this->belongsTo('App\Models\Category', 'parent_id', 'id');
    }

    public function Products()
    {
        return $this->hasMany('App\Models\Product');
    }

    public function scopeParents($query)
    {
        return $this->query()->where('parent_id', 0)->get();
    }

	public function registerMediaConversions()
	{
		parent::registerMediaConversions();
		$this->addMediaConversion('Thumb')
		     ->setManipulations(['w' => 350, 'h' => 350, 'fit' => 'crop'])
		     ->performOnCollections('images');
		$this->addMediaConversion('large')
		     ->setManipulations(['w' => 1170, 'h' => 1170, 'fit' => 'contain'])
		     ->performOnCollections('images');
	}

}
