<?php

namespace App\Models;

use App\Models\Fundation\BaseModel;

class Article extends BaseModel
{
    public $mediaLibraryCollections = ['images', 'downloads'];
    public $translatedAttributes = ['title', 'body','meta_title','meta_description'];
}
