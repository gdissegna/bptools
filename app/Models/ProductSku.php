<?php

namespace App\Models;

use App\Models\Fundation\BaseModel;

class ProductSku extends BaseModel
{
	protected $fillable = [];
	public $translatedAttributes = ['name'];

	public function ProductSkuData()
	{
		return $this->hasMany('App\Models\ProductSku');
	}

	public function Product()
	{
		return $this->belongsTo('App\Models\Product');
	}

	public function getColumnTitlesAttribute()
	{
		return explode(",", $this->columns);
	}

}
